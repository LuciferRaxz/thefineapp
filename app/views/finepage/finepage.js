"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FinePageModel_1 = require("./FinePageModel");
var fineModel;
// Event handler for Page "navigatingTo" event attached in main-page.xml
function navigatingTo(args) {
    /*
    This gets a reference this page’s <Page> UI component. You can
    view the API reference of the Page to see what’s available at
    https://docs.nativescript.org/api-reference/classes/_ui_page_.page.html
    */
    var page = args.object;
    /*
    A page’s bindingContext is an object that should be used to perform
    data binding between XML markup and TypeScript code. Properties
    on the bindingContext can be accessed using the {{ }} syntax in XML.
    In this example, the {{ message }} and {{ onTap }} bindings are resolved
    against the object returned by createViewModel().

    You can learn more about data binding in NativeScript at
    https://docs.nativescript.org/core-concepts/data-binding.
    */
    fineModel = new FinePageModel_1.FinePageModel();
    page.bindingContext = fineModel;
}
exports.navigatingTo = navigatingTo;
function onInsertFine(args) {
    console.log('FIne insert');
    // var doc = document; 
    // let text : HTMLElement = doc.getElementById("fineAmount");
    // console.log('fineAmount :: '+text);
    // let index = args.index; 
    // console.log("args :: "+args); 
    // let obj = fineModel.nameList.getItem(index);
    // console.log("Person Name :: "+obj.personName);
    // console.log("Person Id :: "+obj.personId);
    //let entry:frame.NavigationEntry = {moduleName:'views/detailpage/detailpage', context:obj};
    //frame.topmost().navigate(entry);
    // console.log('Redirect');
    // frame.topmost().navigate("views/finepage/finepage"); 
}
exports.onInsertFine = onInsertFine;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmluZXBhZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmaW5lcGFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQU9BLGlEQUE4QztBQUc5QyxJQUFJLFNBQXVCLENBQUM7QUFFNUIsd0VBQXdFO0FBQ3hFLHNCQUE2QixJQUFlO0lBQ3hDOzs7O01BSUU7SUFDRixJQUFJLElBQUksR0FBUyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBRzdCOzs7Ozs7Ozs7TUFTRTtJQUNGLFNBQVMsR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztJQUVoQyxJQUFJLENBQUMsY0FBYyxHQUFHLFNBQVMsQ0FBQztBQUNwQyxDQUFDO0FBdEJELG9DQXNCQztBQUVELHNCQUE2QixJQUFRO0lBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDM0IsdUJBQXVCO0lBQ3ZCLDZEQUE2RDtJQUM3RCxzQ0FBc0M7SUFDdEMsMkJBQTJCO0lBQzNCLGlDQUFpQztJQUNqQywrQ0FBK0M7SUFDL0MsaURBQWlEO0lBQ2pELDZDQUE2QztJQUM3Qyw0RkFBNEY7SUFDNUYsa0NBQWtDO0lBQ2xDLDJCQUEyQjtJQUMzQix3REFBd0Q7QUFDNUQsQ0FBQztBQWRELG9DQWNDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuSW4gTmF0aXZlU2NyaXB0LCBhIGZpbGUgd2l0aCB0aGUgc2FtZSBuYW1lIGFzIGFuIFhNTCBmaWxlIGlzIGtub3duIGFzXHJcbmEgY29kZS1iZWhpbmQgZmlsZS4gVGhlIGNvZGUtYmVoaW5kIGlzIGEgZ3JlYXQgcGxhY2UgdG8gcGxhY2UgeW91ciB2aWV3XHJcbmxvZ2ljLCBhbmQgdG8gc2V0IHVwIHlvdXIgcGFnZeKAmXMgZGF0YSBiaW5kaW5nLlxyXG4qL1xyXG5pbXBvcnQgeyBFdmVudERhdGEgfSBmcm9tICdkYXRhL29ic2VydmFibGUnO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSAndWkvcGFnZSc7XHJcbmltcG9ydCB7RmluZVBhZ2VNb2RlbH0gZnJvbSAnLi9GaW5lUGFnZU1vZGVsJztcclxuaW1wb3J0ICogYXMgZnJhbWUgZnJvbSBcInVpL2ZyYW1lXCI7XHJcblxyXG5sZXQgZmluZU1vZGVsOkZpbmVQYWdlTW9kZWw7IFxyXG5cclxuLy8gRXZlbnQgaGFuZGxlciBmb3IgUGFnZSBcIm5hdmlnYXRpbmdUb1wiIGV2ZW50IGF0dGFjaGVkIGluIG1haW4tcGFnZS54bWxcclxuZXhwb3J0IGZ1bmN0aW9uIG5hdmlnYXRpbmdUbyhhcmdzOiBFdmVudERhdGEpIHtcclxuICAgIC8qXHJcbiAgICBUaGlzIGdldHMgYSByZWZlcmVuY2UgdGhpcyBwYWdl4oCZcyA8UGFnZT4gVUkgY29tcG9uZW50LiBZb3UgY2FuXHJcbiAgICB2aWV3IHRoZSBBUEkgcmVmZXJlbmNlIG9mIHRoZSBQYWdlIHRvIHNlZSB3aGF04oCZcyBhdmFpbGFibGUgYXRcclxuICAgIGh0dHBzOi8vZG9jcy5uYXRpdmVzY3JpcHQub3JnL2FwaS1yZWZlcmVuY2UvY2xhc3Nlcy9fdWlfcGFnZV8ucGFnZS5odG1sXHJcbiAgICAqL1xyXG4gICAgbGV0IHBhZ2UgPSA8UGFnZT5hcmdzLm9iamVjdDtcclxuXHJcbiAgICBcclxuICAgIC8qXHJcbiAgICBBIHBhZ2XigJlzIGJpbmRpbmdDb250ZXh0IGlzIGFuIG9iamVjdCB0aGF0IHNob3VsZCBiZSB1c2VkIHRvIHBlcmZvcm1cclxuICAgIGRhdGEgYmluZGluZyBiZXR3ZWVuIFhNTCBtYXJrdXAgYW5kIFR5cGVTY3JpcHQgY29kZS4gUHJvcGVydGllc1xyXG4gICAgb24gdGhlIGJpbmRpbmdDb250ZXh0IGNhbiBiZSBhY2Nlc3NlZCB1c2luZyB0aGUge3sgfX0gc3ludGF4IGluIFhNTC5cclxuICAgIEluIHRoaXMgZXhhbXBsZSwgdGhlIHt7IG1lc3NhZ2UgfX0gYW5kIHt7IG9uVGFwIH19IGJpbmRpbmdzIGFyZSByZXNvbHZlZFxyXG4gICAgYWdhaW5zdCB0aGUgb2JqZWN0IHJldHVybmVkIGJ5IGNyZWF0ZVZpZXdNb2RlbCgpLlxyXG5cclxuICAgIFlvdSBjYW4gbGVhcm4gbW9yZSBhYm91dCBkYXRhIGJpbmRpbmcgaW4gTmF0aXZlU2NyaXB0IGF0XHJcbiAgICBodHRwczovL2RvY3MubmF0aXZlc2NyaXB0Lm9yZy9jb3JlLWNvbmNlcHRzL2RhdGEtYmluZGluZy5cclxuICAgICovXHJcbiAgICBmaW5lTW9kZWwgPSBuZXcgRmluZVBhZ2VNb2RlbCgpO1xyXG5cclxuICAgIHBhZ2UuYmluZGluZ0NvbnRleHQgPSBmaW5lTW9kZWw7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBvbkluc2VydEZpbmUoYXJnczphbnkpIHtcclxuICAgIGNvbnNvbGUubG9nKCdGSW5lIGluc2VydCcpO1xyXG4gICAgLy8gdmFyIGRvYyA9IGRvY3VtZW50OyBcclxuICAgIC8vIGxldCB0ZXh0IDogSFRNTEVsZW1lbnQgPSBkb2MuZ2V0RWxlbWVudEJ5SWQoXCJmaW5lQW1vdW50XCIpO1xyXG4gICAgLy8gY29uc29sZS5sb2coJ2ZpbmVBbW91bnQgOjogJyt0ZXh0KTtcclxuICAgIC8vIGxldCBpbmRleCA9IGFyZ3MuaW5kZXg7IFxyXG4gICAgLy8gY29uc29sZS5sb2coXCJhcmdzIDo6IFwiK2FyZ3MpOyBcclxuICAgIC8vIGxldCBvYmogPSBmaW5lTW9kZWwubmFtZUxpc3QuZ2V0SXRlbShpbmRleCk7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhcIlBlcnNvbiBOYW1lIDo6IFwiK29iai5wZXJzb25OYW1lKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKFwiUGVyc29uIElkIDo6IFwiK29iai5wZXJzb25JZCk7XHJcbiAgICAvL2xldCBlbnRyeTpmcmFtZS5OYXZpZ2F0aW9uRW50cnkgPSB7bW9kdWxlTmFtZTondmlld3MvZGV0YWlscGFnZS9kZXRhaWxwYWdlJywgY29udGV4dDpvYmp9O1xyXG4gICAgLy9mcmFtZS50b3Btb3N0KCkubmF2aWdhdGUoZW50cnkpO1xyXG4gICAgLy8gY29uc29sZS5sb2coJ1JlZGlyZWN0Jyk7XHJcbiAgICAvLyBmcmFtZS50b3Btb3N0KCkubmF2aWdhdGUoXCJ2aWV3cy9maW5lcGFnZS9maW5lcGFnZVwiKTsgXHJcbn0iXX0=