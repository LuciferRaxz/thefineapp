import {Observable} from 'data/observable';
import {ObservableArray} from 'data/observable-array';
import http = require('http');
import {AppConstantValues} from './FineAppServices/AppConstantValues';

export class MainViewModel extends Observable {
    private _personList : ObservableArray<IPerson> = new ObservableArray<IPerson>();
    private _counter: number;
    private _message: string;
    // private clientId:string= '3MVG9ZL0ppGP5UrAMylsPkSz0mAWH_jNbXoFekogJEXhQhnt2ZYqcAOn1Jkzzn1qSvV13G3YFd38FiET0FXF4';
    // private clientSecret:string = '8536980590341800865';
    // private userName: string  = 'tarique@trailhead.com';
    // private password: string ='$raxz@lucifer769FYMZSPM04PskBmwMY7DMQp2IA';
    // private accessToken : string = '00D28000001iAuy!AQsAQMixg5lf3nBOS1XnEtJMdemBk7NtQbjOT3bH3YCRzAX6aSUDF.TUzpgmyMv7jhofPMEK43r2vtamSvpiGi9dUBSp5An_';
    constructor() {
        super();
        
        //this.getListOfPersons();
    }

    get personList () {
        return this._personList;
    }
    set personList (value:ObservableArray<IPerson>) {
        this._personList = value;
        this.notifyPropertyChange('personList', value);
    }

    private getListOfPersons() {
        console.log("getListOfPersons");
        //this.getAccessToken();
        http.request({ 
            url: "https://concretio-dev-ed.my.salesforce.com/services/data/v20.0/query/?q=SELECT+id,name,Total_Fine__c+from+Person__c+LIMIT+1",
            method: "GET" ,
            headers:
                {
                    //'Content-Type': 'application/json',
                    Authorization:'Bearer '+AppConstantValues.ACCESS_TOKEN//this.accessToken
                }
            }).then((response) => {
                console.log(response.statusCode);
                if( response.statusCode != 200){
                    this.getAccessToken();
                    return;
                }
                var fetchRecords = response.content.toJSON().records;
                this.createListOfPersons(fetchRecords);
            }, (error)=> {
                console.log('error'+error);
            });
    }
 
    private createListOfPersons(records : any){
        console.log('dgsg');
        console.log(records);
        for(var idx = 0; idx<records.length ; idx++){ 
            var singlePerson :IPerson = {personId: '', personTotalFine:0, personName:''};
            singlePerson.personId = records[idx].Id;
            singlePerson.personName = records[idx].Name;
            singlePerson.personTotalFine = records[idx].Total_Fine__c;
            this._personList.push(singlePerson);
        }     
        
    }
    private getAccessToken(){
        //let req:http.HttpRequestOptions = {}
        var body:string = `grant_type=password&client_id=${AppConstantValues.CLIENT_ID}&client_secret=${AppConstantValues.CLIENT_SECRET}&username=${AppConstantValues.USER_NAME}&password=${AppConstantValues.PASSWORD}`;
        http.request({ 
            url: "https://login.salesforce.com/services/oauth2/token",
            method: "POST" ,
            content: encodeURI(body)
        }).then(function (response) {
                this.accessToken = response.content.toJSON().access_token;
                this.makeCall();
        }, function (e) {
            //// Argument (e) is Error!
        });
    }
    
}