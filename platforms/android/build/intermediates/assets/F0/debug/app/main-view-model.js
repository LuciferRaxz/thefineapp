"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var observable_array_1 = require("data/observable-array");
var http = require("http");
var HelloWorldModel = (function (_super) {
    __extends(HelloWorldModel, _super);
    function HelloWorldModel() {
        var _this = _super.call(this) || this;
        _this._personList = new observable_array_1.ObservableArray();
        _this.clientId = '3MVG9ZL0ppGP5UrAMylsPkSz0mAWH_jNbXoFekogJEXhQhnt2ZYqcAOn1Jkzzn1qSvV13G3YFd38FiET0FXF4';
        _this.clientSecret = '8536980590341800865';
        _this.userName = 'tarique@trailhead.com';
        _this.password = '$raxz@lucifer769FYMZSPM04PskBmwMY7DMQp2IA';
        _this.accessToken = '00D28000001iAuy!AQsAQMixg5lf3nBOS1XnEtJMdemBk7NtQbjOT3bH3YCRzAX6aSUDF.TUzpgmyMv7jhofPMEK43r2vtamSvpiGi9dUBSp5An_';
        return _this;
        //this.getListOfPersons();
    }
    Object.defineProperty(HelloWorldModel.prototype, "personList", {
        get: function () {
            return this._personList;
        },
        set: function (value) {
            this._personList = value;
            this.notifyPropertyChange('personList', value);
        },
        enumerable: true,
        configurable: true
    });
    HelloWorldModel.prototype.getListOfPersons = function () {
        var _this = this;
        console.log("getListOfPersons");
        //this.getAccessToken();
        http.request({
            url: "https://concretio-dev-ed.my.salesforce.com/services/data/v20.0/query/?q=SELECT+id,name,Total_Fine__c+from+Person__c+LIMIT+1",
            method: "GET",
            headers: {
                //'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.accessToken
            }
        }).then(function (response) {
            console.log(response.statusCode);
            if (response.statusCode != 200) {
                _this.getAccessToken();
                return;
            }
            var fetchRecords = response.content.toJSON().records;
            _this.createListOfPersons(fetchRecords);
        }, function (error) {
            console.log('error' + error);
        });
    };
    HelloWorldModel.prototype.createListOfPersons = function (records) {
        console.log('dgsg');
        console.log(records);
        for (var idx = 0; idx < records.length; idx++) {
            var singlePerson = { personId: '', personTotalFine: 0, personName: '' };
            singlePerson.personId = records[idx].Id;
            singlePerson.personName = records[idx].Name;
            singlePerson.personTotalFine = records[idx].Total_Fine__c;
            this._personList.push(singlePerson);
        }
    };
    HelloWorldModel.prototype.getAccessToken = function () {
        //let req:http.HttpRequestOptions = {}
        var body = "grant_type=password&client_id=" + this.clientId + "&client_secret=" + this.clientSecret + "&username=" + this.userName + "&password=" + this.password;
        http.request({
            url: "https://login.salesforce.com/services/oauth2/token",
            method: "POST",
            content: encodeURI(body)
        }).then(function (response) {
            this.accessToken = response.content.toJSON().access_token;
            this.makeCall();
        }, function (e) {
            //// Argument (e) is Error!
        });
    };
    return HelloWorldModel;
}(observable_1.Observable));
exports.HelloWorldModel = HelloWorldModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi12aWV3LW1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibWFpbi12aWV3LW1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsOENBQTJDO0FBQzNDLDBEQUFzRDtBQUN0RCwyQkFBOEI7QUFFOUI7SUFBcUMsbUNBQVU7SUFTM0M7UUFBQSxZQUNJLGlCQUFPLFNBR1Y7UUFaTyxpQkFBVyxHQUE4QixJQUFJLGtDQUFlLEVBQVcsQ0FBQztRQUd4RSxjQUFRLEdBQVMsdUZBQXVGLENBQUM7UUFDekcsa0JBQVksR0FBVSxxQkFBcUIsQ0FBQztRQUM1QyxjQUFRLEdBQVksdUJBQXVCLENBQUM7UUFDNUMsY0FBUSxHQUFVLDJDQUEyQyxDQUFDO1FBQzlELGlCQUFXLEdBQVksa0hBQWtILENBQUM7O1FBSTlJLDBCQUEwQjtJQUM5QixDQUFDO0lBRUQsc0JBQUksdUNBQVU7YUFBZDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVCLENBQUM7YUFDRCxVQUFnQixLQUE4QjtZQUMxQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztZQUN6QixJQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ25ELENBQUM7OztPQUpBO0lBTU8sMENBQWdCLEdBQXhCO1FBQUEsaUJBc0JDO1FBckJHLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNoQyx3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUNULEdBQUcsRUFBRSw2SEFBNkg7WUFDbEksTUFBTSxFQUFFLEtBQUs7WUFDYixPQUFPLEVBQ0g7Z0JBQ0kscUNBQXFDO2dCQUNyQyxhQUFhLEVBQUMsU0FBUyxHQUFDLElBQUksQ0FBQyxXQUFXO2FBQzNDO1NBQ0osQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVE7WUFDYixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNqQyxFQUFFLENBQUEsQ0FBRSxRQUFRLENBQUMsVUFBVSxJQUFJLEdBQUcsQ0FBQyxDQUFBLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDdEIsTUFBTSxDQUFDO1lBQ1gsQ0FBQztZQUNELElBQUksWUFBWSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsT0FBTyxDQUFDO1lBQ3JELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUMzQyxDQUFDLEVBQUUsVUFBQyxLQUFLO1lBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU8sNkNBQW1CLEdBQTNCLFVBQTRCLE9BQWE7UUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JCLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRyxHQUFHLEVBQUUsRUFBQyxDQUFDO1lBQ3pDLElBQUksWUFBWSxHQUFZLEVBQUMsUUFBUSxFQUFFLEVBQUUsRUFBRSxlQUFlLEVBQUMsQ0FBQyxFQUFFLFVBQVUsRUFBQyxFQUFFLEVBQUMsQ0FBQztZQUM3RSxZQUFZLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDeEMsWUFBWSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzVDLFlBQVksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztZQUMxRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN4QyxDQUFDO0lBRUwsQ0FBQztJQUNPLHdDQUFjLEdBQXRCO1FBQ0ksc0NBQXNDO1FBQ3RDLElBQUksSUFBSSxHQUFVLG1DQUFpQyxJQUFJLENBQUMsUUFBUSx1QkFBa0IsSUFBSSxDQUFDLFlBQVksa0JBQWEsSUFBSSxDQUFDLFFBQVEsa0JBQWEsSUFBSSxDQUFDLFFBQVUsQ0FBQztRQUMxSixJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1QsR0FBRyxFQUFFLG9EQUFvRDtZQUN6RCxNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDO1NBQzNCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxRQUFRO1lBQ2xCLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxZQUFZLENBQUM7WUFDMUQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hCLENBQUMsRUFBRSxVQUFVLENBQUM7WUFDViwyQkFBMkI7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUwsc0JBQUM7QUFBRCxDQUFDLEFBMUVELENBQXFDLHVCQUFVLEdBMEU5QztBQTFFWSwwQ0FBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAnZGF0YS9vYnNlcnZhYmxlJztcbmltcG9ydCB7T2JzZXJ2YWJsZUFycmF5fSBmcm9tICdkYXRhL29ic2VydmFibGUtYXJyYXknO1xuaW1wb3J0IGh0dHAgPSByZXF1aXJlKCdodHRwJyk7XG5cbmV4cG9ydCBjbGFzcyBIZWxsb1dvcmxkTW9kZWwgZXh0ZW5kcyBPYnNlcnZhYmxlIHtcbiAgICBwcml2YXRlIF9wZXJzb25MaXN0IDogT2JzZXJ2YWJsZUFycmF5PElQZXJzb24+ID0gbmV3IE9ic2VydmFibGVBcnJheTxJUGVyc29uPigpO1xuICAgIHByaXZhdGUgX2NvdW50ZXI6IG51bWJlcjtcbiAgICBwcml2YXRlIF9tZXNzYWdlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBjbGllbnRJZDpzdHJpbmc9ICczTVZHOVpMMHBwR1A1VXJBTXlsc1BrU3owbUFXSF9qTmJYb0Zla29nSkVYaFFobnQyWllxY0FPbjFKa3p6bjFxU3ZWMTNHM1lGZDM4RmlFVDBGWEY0JztcbiAgICBwcml2YXRlIGNsaWVudFNlY3JldDpzdHJpbmcgPSAnODUzNjk4MDU5MDM0MTgwMDg2NSc7XG4gICAgcHJpdmF0ZSB1c2VyTmFtZTogc3RyaW5nICA9ICd0YXJpcXVlQHRyYWlsaGVhZC5jb20nO1xuICAgIHByaXZhdGUgcGFzc3dvcmQ6IHN0cmluZyA9JyRyYXh6QGx1Y2lmZXI3NjlGWU1aU1BNMDRQc2tCbXdNWTdETVFwMklBJztcbiAgICBwcml2YXRlIGFjY2Vzc1Rva2VuIDogc3RyaW5nID0gJzAwRDI4MDAwMDAxaUF1eSFBUXNBUU1peGc1bGYzbkJPUzFYbkV0Sk1kZW1CazdOdFFiak9UM2JIM1lDUnpBWDZhU1VERi5UVXpwZ215TXY3amhvZlBNRUs0M3IydnRhbVN2cGlHaTlkVUJTcDVBbl8nO1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcigpO1xuICAgICAgICBcbiAgICAgICAgLy90aGlzLmdldExpc3RPZlBlcnNvbnMoKTtcbiAgICB9XG5cbiAgICBnZXQgcGVyc29uTGlzdCAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9wZXJzb25MaXN0O1xuICAgIH1cbiAgICBzZXQgcGVyc29uTGlzdCAodmFsdWU6T2JzZXJ2YWJsZUFycmF5PElQZXJzb24+KSB7XG4gICAgICAgIHRoaXMuX3BlcnNvbkxpc3QgPSB2YWx1ZTtcbiAgICAgICAgdGhpcy5ub3RpZnlQcm9wZXJ0eUNoYW5nZSgncGVyc29uTGlzdCcsIHZhbHVlKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldExpc3RPZlBlcnNvbnMoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiZ2V0TGlzdE9mUGVyc29uc1wiKTtcbiAgICAgICAgLy90aGlzLmdldEFjY2Vzc1Rva2VuKCk7XG4gICAgICAgIGh0dHAucmVxdWVzdCh7IFxuICAgICAgICAgICAgdXJsOiBcImh0dHBzOi8vY29uY3JldGlvLWRldi1lZC5teS5zYWxlc2ZvcmNlLmNvbS9zZXJ2aWNlcy9kYXRhL3YyMC4wL3F1ZXJ5Lz9xPVNFTEVDVCtpZCxuYW1lLFRvdGFsX0ZpbmVfX2MrZnJvbStQZXJzb25fX2MrTElNSVQrMVwiLFxuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiICxcbiAgICAgICAgICAgIGhlYWRlcnM6XG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAvLydDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICAgICAgICAgICAgIEF1dGhvcml6YXRpb246J0JlYXJlciAnK3RoaXMuYWNjZXNzVG9rZW5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlLnN0YXR1c0NvZGUpO1xuICAgICAgICAgICAgICAgIGlmKCByZXNwb25zZS5zdGF0dXNDb2RlICE9IDIwMCl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0QWNjZXNzVG9rZW4oKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB2YXIgZmV0Y2hSZWNvcmRzID0gcmVzcG9uc2UuY29udGVudC50b0pTT04oKS5yZWNvcmRzO1xuICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlTGlzdE9mUGVyc29ucyhmZXRjaFJlY29yZHMpO1xuICAgICAgICAgICAgfSwgKGVycm9yKT0+IHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZXJyb3InK2Vycm9yKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cbiBcbiAgICBwcml2YXRlIGNyZWF0ZUxpc3RPZlBlcnNvbnMocmVjb3JkcyA6IGFueSl7XG4gICAgICAgIGNvbnNvbGUubG9nKCdkZ3NnJyk7XG4gICAgICAgIGNvbnNvbGUubG9nKHJlY29yZHMpO1xuICAgICAgICBmb3IodmFyIGlkeCA9IDA7IGlkeDxyZWNvcmRzLmxlbmd0aCA7IGlkeCsrKXsgXG4gICAgICAgICAgICB2YXIgc2luZ2xlUGVyc29uIDpJUGVyc29uID0ge3BlcnNvbklkOiAnJywgcGVyc29uVG90YWxGaW5lOjAsIHBlcnNvbk5hbWU6Jyd9O1xuICAgICAgICAgICAgc2luZ2xlUGVyc29uLnBlcnNvbklkID0gcmVjb3Jkc1tpZHhdLklkO1xuICAgICAgICAgICAgc2luZ2xlUGVyc29uLnBlcnNvbk5hbWUgPSByZWNvcmRzW2lkeF0uTmFtZTtcbiAgICAgICAgICAgIHNpbmdsZVBlcnNvbi5wZXJzb25Ub3RhbEZpbmUgPSByZWNvcmRzW2lkeF0uVG90YWxfRmluZV9fYztcbiAgICAgICAgICAgIHRoaXMuX3BlcnNvbkxpc3QucHVzaChzaW5nbGVQZXJzb24pO1xuICAgICAgICB9ICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIHByaXZhdGUgZ2V0QWNjZXNzVG9rZW4oKXtcbiAgICAgICAgLy9sZXQgcmVxOmh0dHAuSHR0cFJlcXVlc3RPcHRpb25zID0ge31cbiAgICAgICAgdmFyIGJvZHk6c3RyaW5nID0gYGdyYW50X3R5cGU9cGFzc3dvcmQmY2xpZW50X2lkPSR7dGhpcy5jbGllbnRJZH0mY2xpZW50X3NlY3JldD0ke3RoaXMuY2xpZW50U2VjcmV0fSZ1c2VybmFtZT0ke3RoaXMudXNlck5hbWV9JnBhc3N3b3JkPSR7dGhpcy5wYXNzd29yZH1gO1xuICAgICAgICBodHRwLnJlcXVlc3QoeyBcbiAgICAgICAgICAgIHVybDogXCJodHRwczovL2xvZ2luLnNhbGVzZm9yY2UuY29tL3NlcnZpY2VzL29hdXRoMi90b2tlblwiLFxuICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIiAsXG4gICAgICAgICAgICBjb250ZW50OiBlbmNvZGVVUkkoYm9keSlcbiAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmFjY2Vzc1Rva2VuID0gcmVzcG9uc2UuY29udGVudC50b0pTT04oKS5hY2Nlc3NfdG9rZW47XG4gICAgICAgICAgICAgICAgdGhpcy5tYWtlQ2FsbCgpO1xuICAgICAgICB9LCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgLy8vLyBBcmd1bWVudCAoZSkgaXMgRXJyb3IhXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBcbn0iXX0=