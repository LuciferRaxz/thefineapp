"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var observable_array_1 = require("data/observable-array");
var DetailPageServices_1 = require("../../FineAppServices/DetailPageServices");
var dPServices;
var DetailPageModel = (function (_super) {
    __extends(DetailPageModel, _super);
    function DetailPageModel(personObject) {
        var _this = _super.call(this) || this;
        _this._detailList = new observable_array_1.ObservableArray();
        _this.personObject = new observable_1.Observable();
        _this.personObject = observable_1.fromObject(personObject);
        dPServices = new DetailPageServices_1.DetailPageServices(personObject.personId);
        _this.getListOfPersons();
        return _this;
    }
    Object.defineProperty(DetailPageModel.prototype, "detailList", {
        get: function () {
            return this._detailList;
        },
        set: function (value) {
            this._detailList = value;
            this.notifyPropertyChange('detailList', value);
        },
        enumerable: true,
        configurable: true
    });
    DetailPageModel.prototype.getListOfPersons = function () {
        var _this = this;
        dPServices.fetchDetailList().then(function (response) {
            console.log(response.statusCode);
            if (response.statusCode != 200) {
                dPServices.getAccessToken();
                _this.getListOfPersons();
            }
            _this.detailList = dPServices.createDetailList(response.content.toJSON().records);
        });
    };
    return DetailPageModel;
}(observable_1.Observable));
exports.DetailPageModel = DetailPageModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGV0YWlsUGFnZU1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiRGV0YWlsUGFnZU1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsOENBQXNEO0FBQ3RELDBEQUFzRDtBQUN0RCwrRUFBOEU7QUFHOUUsSUFBSSxVQUErQixDQUFDO0FBR3BDO0lBQXFDLG1DQUFVO0lBRzNDLHlCQUFZLFlBQW9CO1FBQWhDLFlBQ0ksaUJBQU8sU0FJVjtRQVBPLGlCQUFXLEdBQThCLElBQUksa0NBQWUsRUFBVyxDQUFDO1FBQ3hFLGtCQUFZLEdBQWdCLElBQUksdUJBQVUsRUFBRSxDQUFDO1FBR2pELEtBQUksQ0FBQyxZQUFZLEdBQUksdUJBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM5QyxVQUFVLEdBQUcsSUFBSSx1Q0FBa0IsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDM0QsS0FBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7O0lBQzVCLENBQUM7SUFFRCxzQkFBSSx1Q0FBVTthQUFkO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDNUIsQ0FBQzthQUNELFVBQWdCLEtBQThCO1lBQzFDLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbkQsQ0FBQzs7O09BSkE7SUFNTywwQ0FBZ0IsR0FBeEI7UUFBQSxpQkFVQztRQVRHLFVBQVUsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRO1lBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2pDLEVBQUUsQ0FBQSxDQUFFLFFBQVEsQ0FBQyxVQUFVLElBQUksR0FBRyxDQUFDLENBQUEsQ0FBQztnQkFDNUIsVUFBVSxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUM1QixLQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUM1QixDQUFDO1lBQ0QsS0FBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyRixDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFDTCxzQkFBQztBQUFELENBQUMsQUE3QkQsQ0FBcUMsdUJBQVUsR0E2QjlDO0FBN0JZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtPYnNlcnZhYmxlLGZyb21PYmplY3R9IGZyb20gJ2RhdGEvb2JzZXJ2YWJsZSc7XHJcbmltcG9ydCB7T2JzZXJ2YWJsZUFycmF5fSBmcm9tICdkYXRhL29ic2VydmFibGUtYXJyYXknO1xyXG5pbXBvcnQgeyBEZXRhaWxQYWdlU2VydmljZXMgfSBmcm9tICcuLi8uLi9GaW5lQXBwU2VydmljZXMvRGV0YWlsUGFnZVNlcnZpY2VzJztcclxuaW1wb3J0IGh0dHAgPSByZXF1aXJlKCdodHRwJyk7XHJcblxyXG5sZXQgZFBTZXJ2aWNlcyA6IERldGFpbFBhZ2VTZXJ2aWNlcztcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgRGV0YWlsUGFnZU1vZGVsIGV4dGVuZHMgT2JzZXJ2YWJsZXtcclxuICAgIHByaXZhdGUgX2RldGFpbExpc3QgOiBPYnNlcnZhYmxlQXJyYXk8SURldGFpbD4gPSBuZXcgT2JzZXJ2YWJsZUFycmF5PElEZXRhaWw+KCk7XHJcbiAgICBwcml2YXRlIHBlcnNvbk9iamVjdCA6IE9ic2VydmFibGUgPSBuZXcgT2JzZXJ2YWJsZSgpO1xyXG4gICAgY29uc3RydWN0b3IocGVyc29uT2JqZWN0OklQZXJzb24pIHtcclxuICAgICAgICBzdXBlcigpOyBcclxuICAgICAgICB0aGlzLnBlcnNvbk9iamVjdCA9ICBmcm9tT2JqZWN0KHBlcnNvbk9iamVjdCk7XHJcbiAgICAgICAgZFBTZXJ2aWNlcyA9IG5ldyBEZXRhaWxQYWdlU2VydmljZXMocGVyc29uT2JqZWN0LnBlcnNvbklkKTtcclxuICAgICAgICB0aGlzLmdldExpc3RPZlBlcnNvbnMoKTtcclxuICAgIH1cclxuIFxyXG4gICAgZ2V0IGRldGFpbExpc3QgKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kZXRhaWxMaXN0O1xyXG4gICAgfVxyXG4gICAgc2V0IGRldGFpbExpc3QgKHZhbHVlOk9ic2VydmFibGVBcnJheTxJRGV0YWlsPikge1xyXG4gICAgICAgIHRoaXMuX2RldGFpbExpc3QgPSB2YWx1ZTsgXHJcbiAgICAgICAgdGhpcy5ub3RpZnlQcm9wZXJ0eUNoYW5nZSgnZGV0YWlsTGlzdCcsIHZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldExpc3RPZlBlcnNvbnMoKSB7XHJcbiAgICAgICAgZFBTZXJ2aWNlcy5mZXRjaERldGFpbExpc3QoKS50aGVuKChyZXNwb25zZSk9PnsgXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlLnN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICBpZiggcmVzcG9uc2Uuc3RhdHVzQ29kZSAhPSAyMDApe1xyXG4gICAgICAgICAgICAgICAgZFBTZXJ2aWNlcy5nZXRBY2Nlc3NUb2tlbigpOyBcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0TGlzdE9mUGVyc29ucygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuZGV0YWlsTGlzdCA9IGRQU2VydmljZXMuY3JlYXRlRGV0YWlsTGlzdChyZXNwb25zZS5jb250ZW50LnRvSlNPTigpLnJlY29yZHMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG59Il19