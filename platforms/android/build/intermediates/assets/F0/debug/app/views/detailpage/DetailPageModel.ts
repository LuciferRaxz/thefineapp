import {Observable,fromObject} from 'data/observable';
import {ObservableArray} from 'data/observable-array';
import { DetailPageServices } from '../../FineAppServices/DetailPageServices';
import http = require('http');

let dPServices : DetailPageServices;


export class DetailPageModel extends Observable{
    private _detailList : ObservableArray<IDetail> = new ObservableArray<IDetail>();
    private personObject : Observable = new Observable();
    constructor(personObject:IPerson) {
        super(); 
        this.personObject =  fromObject(personObject);
        dPServices = new DetailPageServices(personObject.personId);
        this.getListOfPersons();
    }
 
    get detailList () {
        return this._detailList;
    }
    set detailList (value:ObservableArray<IDetail>) {
        this._detailList = value; 
        this.notifyPropertyChange('detailList', value);
    }

    private getListOfPersons() {
        dPServices.fetchDetailList().then((response)=>{ 
            console.log(response.statusCode);
            if( response.statusCode != 200){
                dPServices.getAccessToken(); 
                this.getListOfPersons();
            }
            this.detailList = dPServices.createDetailList(response.content.toJSON().records);
        });
        
    }
}