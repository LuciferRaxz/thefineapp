"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var observable_array_1 = require("data/observable-array");
var HttpUtility_1 = require("../HttpUtility/HttpUtility");
var httpUtility;
var FinePageServices = (function (_super) {
    __extends(FinePageServices, _super);
    function FinePageServices() {
        var _this = _super.call(this) || this;
        //Necessary variables reqired for the Access token
        _this.clientId = '3MVG9ZL0ppGP5UrAMylsPkSz0mAWH_jNbXoFekogJEXhQhnt2ZYqcAOn1Jkzzn1qSvV13G3YFd38FiET0FXF4';
        _this.clientSecret = '8536980590341800865';
        _this.userName = 'tarique@trailhead.com';
        _this.password = '$raxz@lucifer769FYMZSPM04PskBmwMY7DMQp2IA';
        _this.accessToken = '00D28000001iAuy!AQsAQKOy.YXl1ivZeusr6TxiS2SUmJTpTCaAyLOw.wJbq7.VhhxwOdoCYLCMyhSUl2zIeujt.IorFyCtsBSXsDMSKo59IMU4';
        httpUtility = new HttpUtility_1.HttpUtility();
        return _this;
    }
    FinePageServices.prototype.fetchNameList = function () {
        var query = "SELECT+Name+FROM+Person__c";
        return httpUtility.getMethod(query, this.accessToken);
    };
    FinePageServices.prototype.createDetailList = function (records) {
        var nameList = new observable_array_1.ObservableArray();
        for (var idx = 0; idx < records.length; idx++) {
            //var singleDetail :IName = {personId:'', personName:''};
            //singleDetail.personName = records[idx].Name;
            nameList.push(records[idx].Name);
        }
        return nameList;
    };
    FinePageServices.prototype.getAccessToken = function () {
        var _this = this;
        var body = "grant_type=password&client_id=" + this.clientId + "&client_secret=" + this.clientSecret + "&username=" + this.userName + "&password=" + this.password;
        var postResponse = httpUtility.postMethod(body);
        postResponse.then(function (response) {
            _this.accessToken = response.content.toJSON().access_token;
            _this.fetchNameList();
        }, function (error) {
            //// Argument (e) is Error!
            console.log(error);
        });
    };
    return FinePageServices;
}(observable_1.Observable));
exports.FinePageServices = FinePageServices;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRmluZVBhZ2VTZXJ2aWNlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkZpbmVQYWdlU2VydmljZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw4Q0FBa0U7QUFDbEUsMERBQXNEO0FBQ3RELDBEQUFzRDtBQUd0RCxJQUFJLFdBQXlCLENBQUM7QUFFOUI7SUFBc0Msb0NBQVU7SUFTNUM7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFWRCxrREFBa0Q7UUFDMUMsY0FBUSxHQUFTLHVGQUF1RixDQUFDO1FBQ3pHLGtCQUFZLEdBQVUscUJBQXFCLENBQUM7UUFDNUMsY0FBUSxHQUFZLHVCQUF1QixDQUFDO1FBQzVDLGNBQVEsR0FBVSwyQ0FBMkMsQ0FBQztRQUM5RCxpQkFBVyxHQUFZLGtIQUFrSCxDQUFDO1FBSTlJLFdBQVcsR0FBRyxJQUFJLHlCQUFXLEVBQUUsQ0FBQzs7SUFDcEMsQ0FBQztJQUVNLHdDQUFhLEdBQXBCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsNEJBQTRCLENBQUM7UUFDekMsTUFBTSxDQUFFLFdBQVcsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRU0sMkNBQWdCLEdBQXZCLFVBQXdCLE9BQWE7UUFDakMsSUFBSSxRQUFRLEdBQUcsSUFBSSxrQ0FBZSxFQUFTLENBQUM7UUFDNUMsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBQyxPQUFPLENBQUMsTUFBTSxFQUFHLEdBQUcsRUFBRSxFQUFDLENBQUM7WUFDekMseURBQXlEO1lBQ3pELDhDQUE4QztZQUM5QyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRU0seUNBQWMsR0FBckI7UUFBQSxpQkFVQztRQVRHLElBQUksSUFBSSxHQUFVLG1DQUFpQyxJQUFJLENBQUMsUUFBUSx1QkFBa0IsSUFBSSxDQUFDLFlBQVksa0JBQWEsSUFBSSxDQUFDLFFBQVEsa0JBQWEsSUFBSSxDQUFDLFFBQVUsQ0FBQztRQUMxSixJQUFJLFlBQVksR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hELFlBQVksQ0FBQyxJQUFJLENBQUUsVUFBQyxRQUFRO1lBQ3BCLEtBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxZQUFZLENBQUM7WUFDMUQsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQzdCLENBQUMsRUFBRSxVQUFDLEtBQUs7WUFDTCwyQkFBMkI7WUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTCx1QkFBQztBQUFELENBQUMsQUF6Q0QsQ0FBc0MsdUJBQVUsR0F5Qy9DO0FBekNZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7T2JzZXJ2YWJsZSwgZnJvbU9iamVjdCwgRXZlbnREYXRhfSBmcm9tICdkYXRhL29ic2VydmFibGUnO1xyXG5pbXBvcnQge09ic2VydmFibGVBcnJheX0gZnJvbSAnZGF0YS9vYnNlcnZhYmxlLWFycmF5JztcclxuaW1wb3J0IHtIdHRwVXRpbGl0eX0gZnJvbSAnLi4vSHR0cFV0aWxpdHkvSHR0cFV0aWxpdHknXHJcbmltcG9ydCBodHRwID0gcmVxdWlyZSgnaHR0cCcpO1xyXG5cclxubGV0IGh0dHBVdGlsaXR5IDogSHR0cFV0aWxpdHk7XHJcbiBcclxuZXhwb3J0IGNsYXNzIEZpbmVQYWdlU2VydmljZXMgZXh0ZW5kcyBPYnNlcnZhYmxlIHtcclxuXHJcbiAgICAvL05lY2Vzc2FyeSB2YXJpYWJsZXMgcmVxaXJlZCBmb3IgdGhlIEFjY2VzcyB0b2tlblxyXG4gICAgcHJpdmF0ZSBjbGllbnRJZDpzdHJpbmc9ICczTVZHOVpMMHBwR1A1VXJBTXlsc1BrU3owbUFXSF9qTmJYb0Zla29nSkVYaFFobnQyWllxY0FPbjFKa3p6bjFxU3ZWMTNHM1lGZDM4RmlFVDBGWEY0JztcclxuICAgIHByaXZhdGUgY2xpZW50U2VjcmV0OnN0cmluZyA9ICc4NTM2OTgwNTkwMzQxODAwODY1JztcclxuICAgIHByaXZhdGUgdXNlck5hbWU6IHN0cmluZyAgPSAndGFyaXF1ZUB0cmFpbGhlYWQuY29tJztcclxuICAgIHByaXZhdGUgcGFzc3dvcmQ6IHN0cmluZyA9JyRyYXh6QGx1Y2lmZXI3NjlGWU1aU1BNMDRQc2tCbXdNWTdETVFwMklBJztcclxuICAgIHByaXZhdGUgYWNjZXNzVG9rZW4gOiBzdHJpbmcgPSAnMDBEMjgwMDAwMDFpQXV5IUFRc0FRS095LllYbDFpdlpldXNyNlR4aVMyU1VtSlRwVENhQXlMT3cud0picTcuVmhoeHdPZG9DWUxDTXloU1VsMnpJZXVqdC5Jb3JGeUN0c0JTWHNETVNLbzU5SU1VNCc7XHJcbiAgICBcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgaHR0cFV0aWxpdHkgPSBuZXcgSHR0cFV0aWxpdHkoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZmV0Y2hOYW1lTGlzdCgpIDogUHJvbWlzZTxodHRwLkh0dHBSZXNwb25zZT57XHJcbiAgICAgICAgdmFyIHF1ZXJ5ID0gXCJTRUxFQ1QrTmFtZStGUk9NK1BlcnNvbl9fY1wiO1xyXG4gICAgICAgIHJldHVybiAgaHR0cFV0aWxpdHkuZ2V0TWV0aG9kKHF1ZXJ5LHRoaXMuYWNjZXNzVG9rZW4pO1xyXG4gICAgfVxyXG4gXHJcbiAgICBwdWJsaWMgY3JlYXRlRGV0YWlsTGlzdChyZWNvcmRzIDogYW55KSA6IE9ic2VydmFibGVBcnJheTxJTmFtZT57XHJcbiAgICAgICAgbGV0IG5hbWVMaXN0ID0gbmV3IE9ic2VydmFibGVBcnJheTxJTmFtZT4oKTtcclxuICAgICAgICBmb3IodmFyIGlkeCA9IDA7IGlkeDxyZWNvcmRzLmxlbmd0aCA7IGlkeCsrKXsgXHJcbiAgICAgICAgICAgIC8vdmFyIHNpbmdsZURldGFpbCA6SU5hbWUgPSB7cGVyc29uSWQ6JycsIHBlcnNvbk5hbWU6Jyd9O1xyXG4gICAgICAgICAgICAvL3NpbmdsZURldGFpbC5wZXJzb25OYW1lID0gcmVjb3Jkc1tpZHhdLk5hbWU7XHJcbiAgICAgICAgICAgIG5hbWVMaXN0LnB1c2gocmVjb3Jkc1tpZHhdLk5hbWUpO1xyXG4gICAgICAgIH0gICAgIFxyXG4gICAgICAgIHJldHVybiBuYW1lTGlzdDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0QWNjZXNzVG9rZW4oKXtcclxuICAgICAgICB2YXIgYm9keTpzdHJpbmcgPSBgZ3JhbnRfdHlwZT1wYXNzd29yZCZjbGllbnRfaWQ9JHt0aGlzLmNsaWVudElkfSZjbGllbnRfc2VjcmV0PSR7dGhpcy5jbGllbnRTZWNyZXR9JnVzZXJuYW1lPSR7dGhpcy51c2VyTmFtZX0mcGFzc3dvcmQ9JHt0aGlzLnBhc3N3b3JkfWA7XHJcbiAgICAgICAgdmFyIHBvc3RSZXNwb25zZSA9IGh0dHBVdGlsaXR5LnBvc3RNZXRob2QoYm9keSk7XHJcbiAgICAgICAgcG9zdFJlc3BvbnNlLnRoZW4oIChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hY2Nlc3NUb2tlbiA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCkuYWNjZXNzX3Rva2VuO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mZXRjaE5hbWVMaXN0KCk7XHJcbiAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIC8vLy8gQXJndW1lbnQgKGUpIGlzIEVycm9yIVxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBcclxufSJdfQ==