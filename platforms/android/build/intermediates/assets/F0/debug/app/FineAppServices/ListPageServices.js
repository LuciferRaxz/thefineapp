"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var observable_array_1 = require("data/observable-array");
var HttpUtility_1 = require("../HttpUtility/HttpUtility");
var httpUtility;
var ListPageServices = (function (_super) {
    __extends(ListPageServices, _super);
    function ListPageServices() {
        var _this = _super.call(this) || this;
        //Necessary variables reqired for the Access token
        _this.clientId = '3MVG9ZL0ppGP5UrAMylsPkSz0mAWH_jNbXoFekogJEXhQhnt2ZYqcAOn1Jkzzn1qSvV13G3YFd38FiET0FXF4';
        _this.clientSecret = '8536980590341800865';
        _this.userName = 'tarique@trailhead.com';
        _this.password = '$raxz@lucifer769FYMZSPM04PskBmwMY7DMQp2IA';
        _this.accessToken = '00D28000001iAuy!AQsAQKOy.YXl1ivZeusr6TxiS2SUmJTpTCaAyLOw.wJbq7.VhhxwOdoCYLCMyhSUl2zIeujt.IorFyCtsBSXsDMSKo59IMU4';
        httpUtility = new HttpUtility_1.HttpUtility();
        return _this;
    }
    Object.defineProperty(ListPageServices.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (value) {
            this._data = value;
            this.notifyPropertyChange('data', value);
        },
        enumerable: true,
        configurable: true
    });
    ListPageServices.prototype.fetchListOfPersons = function () {
        var personList = new observable_array_1.ObservableArray();
        var query = "SELECT+id,name,Total_Fine__c+from+Person__c";
        return httpUtility.getMethod(query, this.accessToken);
    };
    ListPageServices.prototype.createListOfPersons = function (records) {
        var personList = new observable_array_1.ObservableArray();
        for (var idx = 0; idx < records.length; idx++) {
            var singlePerson = { personId: '', personTotalFine: 0, personName: '' };
            singlePerson.personId = records[idx].Id;
            singlePerson.personName = records[idx].Name;
            singlePerson.personTotalFine = records[idx].Total_Fine__c;
            personList.push(singlePerson);
        }
        return personList;
    };
    ListPageServices.prototype.getAccessToken = function () {
        var _this = this;
        var body = "grant_type=password&client_id=" + this.clientId + "&client_secret=" + this.clientSecret + "&username=" + this.userName + "&password=" + this.password;
        var postResponse = httpUtility.postMethod(body);
        postResponse.then(function (response) {
            _this.accessToken = response.content.toJSON().access_token;
            _this.fetchListOfPersons();
        }, function (error) {
            //// Argument (e) is Error!
            console.log(error);
        });
    };
    return ListPageServices;
}(observable_1.Observable));
exports.ListPageServices = ListPageServices;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGlzdFBhZ2VTZXJ2aWNlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkxpc3RQYWdlU2VydmljZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw4Q0FBa0U7QUFDbEUsMERBQXNEO0FBQ3RELDBEQUFzRDtBQU10RCxJQUFJLFdBQXlCLENBQUM7QUFFOUI7SUFBc0Msb0NBQVU7SUFTNUM7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFWRCxrREFBa0Q7UUFDMUMsY0FBUSxHQUFTLHVGQUF1RixDQUFDO1FBQ3pHLGtCQUFZLEdBQVUscUJBQXFCLENBQUM7UUFDNUMsY0FBUSxHQUFZLHVCQUF1QixDQUFDO1FBQzVDLGNBQVEsR0FBVSwyQ0FBMkMsQ0FBQztRQUM5RCxpQkFBVyxHQUFZLGtIQUFrSCxDQUFDO1FBSTlJLFdBQVcsR0FBRyxJQUFJLHlCQUFXLEVBQUUsQ0FBQzs7SUFDcEMsQ0FBQztJQUVELHNCQUFJLGtDQUFJO2FBSVI7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDO2FBTkQsVUFBUyxLQUE4QjtZQUNuQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNuQixJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzdDLENBQUM7OztPQUFBO0lBS00sNkNBQWtCLEdBQXpCO1FBQ0ksSUFBSSxVQUFVLEdBQUcsSUFBSSxrQ0FBZSxFQUFXLENBQUM7UUFDaEQsSUFBSSxLQUFLLEdBQUcsNkNBQTZDLENBQUM7UUFDMUQsTUFBTSxDQUFFLFdBQVcsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRU0sOENBQW1CLEdBQTFCLFVBQTJCLE9BQWE7UUFDcEMsSUFBSSxVQUFVLEdBQUcsSUFBSSxrQ0FBZSxFQUFXLENBQUM7UUFDaEQsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBQyxPQUFPLENBQUMsTUFBTSxFQUFHLEdBQUcsRUFBRSxFQUFDLENBQUM7WUFDekMsSUFBSSxZQUFZLEdBQVksRUFBQyxRQUFRLEVBQUUsRUFBRSxFQUFFLGVBQWUsRUFBQyxDQUFDLEVBQUUsVUFBVSxFQUFDLEVBQUUsRUFBQyxDQUFDO1lBQzdFLFlBQVksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUN4QyxZQUFZLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDNUMsWUFBWSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1lBQzFELFVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDbEMsQ0FBQztRQUNELE1BQU0sQ0FBQyxVQUFVLENBQUM7SUFDdEIsQ0FBQztJQUVNLHlDQUFjLEdBQXJCO1FBQUEsaUJBVUM7UUFURyxJQUFJLElBQUksR0FBVSxtQ0FBaUMsSUFBSSxDQUFDLFFBQVEsdUJBQWtCLElBQUksQ0FBQyxZQUFZLGtCQUFhLElBQUksQ0FBQyxRQUFRLGtCQUFhLElBQUksQ0FBQyxRQUFVLENBQUM7UUFDMUosSUFBSSxZQUFZLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoRCxZQUFZLENBQUMsSUFBSSxDQUFFLFVBQUMsUUFBUTtZQUNwQixLQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsWUFBWSxDQUFDO1lBQzFELEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQ2xDLENBQUMsRUFBRSxVQUFDLEtBQUs7WUFDTCwyQkFBMkI7WUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTCx1QkFBQztBQUFELENBQUMsQUFwREQsQ0FBc0MsdUJBQVUsR0FvRC9DO0FBcERZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7T2JzZXJ2YWJsZSwgZnJvbU9iamVjdCwgRXZlbnREYXRhfSBmcm9tICdkYXRhL29ic2VydmFibGUnO1xyXG5pbXBvcnQge09ic2VydmFibGVBcnJheX0gZnJvbSAnZGF0YS9vYnNlcnZhYmxlLWFycmF5JztcclxuaW1wb3J0IHtIdHRwVXRpbGl0eX0gZnJvbSAnLi4vSHR0cFV0aWxpdHkvSHR0cFV0aWxpdHknXHJcbmltcG9ydCBodHRwID0gcmVxdWlyZSgnaHR0cCcpO1xyXG5pbXBvcnQge0xpc3RQYWdlTW9kZWx9IGZyb20gJy4uL3ZpZXdzL2xpc3RwYWdlL0xpc3RQYWdlTW9kZWwnO1xyXG5cclxuXHJcblxyXG5sZXQgaHR0cFV0aWxpdHkgOiBIdHRwVXRpbGl0eTtcclxuIFxyXG5leHBvcnQgY2xhc3MgTGlzdFBhZ2VTZXJ2aWNlcyBleHRlbmRzIE9ic2VydmFibGUge1xyXG5cclxuICAgIC8vTmVjZXNzYXJ5IHZhcmlhYmxlcyByZXFpcmVkIGZvciB0aGUgQWNjZXNzIHRva2VuXHJcbiAgICBwcml2YXRlIGNsaWVudElkOnN0cmluZz0gJzNNVkc5WkwwcHBHUDVVckFNeWxzUGtTejBtQVdIX2pOYlhvRmVrb2dKRVhoUWhudDJaWXFjQU9uMUprenpuMXFTdlYxM0czWUZkMzhGaUVUMEZYRjQnO1xyXG4gICAgcHJpdmF0ZSBjbGllbnRTZWNyZXQ6c3RyaW5nID0gJzg1MzY5ODA1OTAzNDE4MDA4NjUnO1xyXG4gICAgcHJpdmF0ZSB1c2VyTmFtZTogc3RyaW5nICA9ICd0YXJpcXVlQHRyYWlsaGVhZC5jb20nO1xyXG4gICAgcHJpdmF0ZSBwYXNzd29yZDogc3RyaW5nID0nJHJheHpAbHVjaWZlcjc2OUZZTVpTUE0wNFBza0Jtd01ZN0RNUXAySUEnO1xyXG4gICAgcHJpdmF0ZSBhY2Nlc3NUb2tlbiA6IHN0cmluZyA9ICcwMEQyODAwMDAwMWlBdXkhQVFzQVFLT3kuWVhsMWl2WmV1c3I2VHhpUzJTVW1KVHBUQ2FBeUxPdy53SmJxNy5WaGh4d09kb0NZTENNeWhTVWwyeklldWp0LklvckZ5Q3RzQlNYc0RNU0tvNTlJTVU0JztcclxuICAgIHByaXZhdGUgX2RhdGE6T2JzZXJ2YWJsZUFycmF5PElQZXJzb24+O1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBodHRwVXRpbGl0eSA9IG5ldyBIdHRwVXRpbGl0eSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBkYXRhKHZhbHVlOk9ic2VydmFibGVBcnJheTxJUGVyc29uPikge1xyXG4gICAgICAgIHRoaXMuX2RhdGEgPSB2YWx1ZTtcclxuICAgICAgICB0aGlzLm5vdGlmeVByb3BlcnR5Q2hhbmdlKCdkYXRhJywgdmFsdWUpO1xyXG4gICAgfVxyXG4gICAgZ2V0IGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGZldGNoTGlzdE9mUGVyc29ucygpIDogUHJvbWlzZTxodHRwLkh0dHBSZXNwb25zZT57XHJcbiAgICAgICAgbGV0IHBlcnNvbkxpc3QgPSBuZXcgT2JzZXJ2YWJsZUFycmF5PElQZXJzb24+KCk7XHJcbiAgICAgICAgdmFyIHF1ZXJ5ID0gXCJTRUxFQ1QraWQsbmFtZSxUb3RhbF9GaW5lX19jK2Zyb20rUGVyc29uX19jXCI7XHJcbiAgICAgICAgcmV0dXJuICBodHRwVXRpbGl0eS5nZXRNZXRob2QocXVlcnksdGhpcy5hY2Nlc3NUb2tlbik7XHJcbiAgICB9XHJcbiBcclxuICAgIHB1YmxpYyBjcmVhdGVMaXN0T2ZQZXJzb25zKHJlY29yZHMgOiBhbnkpIDogT2JzZXJ2YWJsZUFycmF5PElQZXJzb24+e1xyXG4gICAgICAgIGxldCBwZXJzb25MaXN0ID0gbmV3IE9ic2VydmFibGVBcnJheTxJUGVyc29uPigpO1xyXG4gICAgICAgIGZvcih2YXIgaWR4ID0gMDsgaWR4PHJlY29yZHMubGVuZ3RoIDsgaWR4KyspeyBcclxuICAgICAgICAgICAgdmFyIHNpbmdsZVBlcnNvbiA6SVBlcnNvbiA9IHtwZXJzb25JZDogJycsIHBlcnNvblRvdGFsRmluZTowLCBwZXJzb25OYW1lOicnfTtcclxuICAgICAgICAgICAgc2luZ2xlUGVyc29uLnBlcnNvbklkID0gcmVjb3Jkc1tpZHhdLklkO1xyXG4gICAgICAgICAgICBzaW5nbGVQZXJzb24ucGVyc29uTmFtZSA9IHJlY29yZHNbaWR4XS5OYW1lO1xyXG4gICAgICAgICAgICBzaW5nbGVQZXJzb24ucGVyc29uVG90YWxGaW5lID0gcmVjb3Jkc1tpZHhdLlRvdGFsX0ZpbmVfX2M7XHJcbiAgICAgICAgICAgIHBlcnNvbkxpc3QucHVzaChzaW5nbGVQZXJzb24pO1xyXG4gICAgICAgIH0gICAgIFxyXG4gICAgICAgIHJldHVybiBwZXJzb25MaXN0O1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRBY2Nlc3NUb2tlbigpe1xyXG4gICAgICAgIHZhciBib2R5OnN0cmluZyA9IGBncmFudF90eXBlPXBhc3N3b3JkJmNsaWVudF9pZD0ke3RoaXMuY2xpZW50SWR9JmNsaWVudF9zZWNyZXQ9JHt0aGlzLmNsaWVudFNlY3JldH0mdXNlcm5hbWU9JHt0aGlzLnVzZXJOYW1lfSZwYXNzd29yZD0ke3RoaXMucGFzc3dvcmR9YDtcclxuICAgICAgICB2YXIgcG9zdFJlc3BvbnNlID0gaHR0cFV0aWxpdHkucG9zdE1ldGhvZChib2R5KTtcclxuICAgICAgICBwb3N0UmVzcG9uc2UudGhlbiggKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFjY2Vzc1Rva2VuID0gcmVzcG9uc2UuY29udGVudC50b0pTT04oKS5hY2Nlc3NfdG9rZW47XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZldGNoTGlzdE9mUGVyc29ucygpO1xyXG4gICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAvLy8vIEFyZ3VtZW50IChlKSBpcyBFcnJvciFcclxuICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgXHJcbn0iXX0=