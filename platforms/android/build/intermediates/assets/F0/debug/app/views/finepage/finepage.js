"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FinePageModel_1 = require("./FinePageModel");
var fineModel;
// Event handler for Page "navigatingTo" event attached in main-page.xml
function navigatingTo(args) {
    /*
    This gets a reference this page’s <Page> UI component. You can
    view the API reference of the Page to see what’s available at
    https://docs.nativescript.org/api-reference/classes/_ui_page_.page.html
    */
    var page = args.object;
    /*
    A page’s bindingContext is an object that should be used to perform
    data binding between XML markup and TypeScript code. Properties
    on the bindingContext can be accessed using the {{ }} syntax in XML.
    In this example, the {{ message }} and {{ onTap }} bindings are resolved
    against the object returned by createViewModel().

    You can learn more about data binding in NativeScript at
    https://docs.nativescript.org/core-concepts/data-binding.
    */
    fineModel = new FinePageModel_1.FinePageModel();
    page.bindingContext = fineModel;
}
exports.navigatingTo = navigatingTo;
function onInsertFine(args) {
    // let text = .getElementById("tf").innerText;
    // console.log(text);
    // let index = args.index; 
    // console.log("index :: "+index);
    // let obj = fineModel.nameList.getItem(index);
    // console.log("Person Name :: "+obj.personName);
    // console.log("Person Id :: "+obj.personId);
    //let entry:frame.NavigationEntry = {moduleName:'views/detailpage/detailpage', context:obj};
    //frame.topmost().navigate(entry);
    // console.log('Redirect');
    // frame.topmost().navigate("views/finepage/finepage"); 
}
exports.onInsertFine = onInsertFine;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmluZXBhZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmaW5lcGFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQU9BLGlEQUE4QztBQUk5QyxJQUFJLFNBQXVCLENBQUM7QUFFNUIsd0VBQXdFO0FBQ3hFLHNCQUE2QixJQUFlO0lBQ3hDOzs7O01BSUU7SUFDRixJQUFJLElBQUksR0FBUyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBRzdCOzs7Ozs7Ozs7TUFTRTtJQUNGLFNBQVMsR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztJQUVoQyxJQUFJLENBQUMsY0FBYyxHQUFHLFNBQVMsQ0FBQztBQUNwQyxDQUFDO0FBdEJELG9DQXNCQztBQUVELHNCQUE2QixJQUFRO0lBQ2pDLDhDQUE4QztJQUM5QyxxQkFBcUI7SUFDckIsMkJBQTJCO0lBQzNCLGtDQUFrQztJQUNsQywrQ0FBK0M7SUFDL0MsaURBQWlEO0lBQ2pELDZDQUE2QztJQUM3Qyw0RkFBNEY7SUFDNUYsa0NBQWtDO0lBQ2xDLDJCQUEyQjtJQUMzQix3REFBd0Q7QUFDNUQsQ0FBQztBQVpELG9DQVlDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuSW4gTmF0aXZlU2NyaXB0LCBhIGZpbGUgd2l0aCB0aGUgc2FtZSBuYW1lIGFzIGFuIFhNTCBmaWxlIGlzIGtub3duIGFzXHJcbmEgY29kZS1iZWhpbmQgZmlsZS4gVGhlIGNvZGUtYmVoaW5kIGlzIGEgZ3JlYXQgcGxhY2UgdG8gcGxhY2UgeW91ciB2aWV3XHJcbmxvZ2ljLCBhbmQgdG8gc2V0IHVwIHlvdXIgcGFnZeKAmXMgZGF0YSBiaW5kaW5nLlxyXG4qL1xyXG5pbXBvcnQgeyBFdmVudERhdGEgfSBmcm9tICdkYXRhL29ic2VydmFibGUnO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSAndWkvcGFnZSc7XHJcbmltcG9ydCB7RmluZVBhZ2VNb2RlbH0gZnJvbSAnLi9GaW5lUGFnZU1vZGVsJztcclxuaW1wb3J0ICogYXMgZnJhbWUgZnJvbSBcInVpL2ZyYW1lXCI7XHJcblxyXG5cclxubGV0IGZpbmVNb2RlbDpGaW5lUGFnZU1vZGVsOyBcclxuXHJcbi8vIEV2ZW50IGhhbmRsZXIgZm9yIFBhZ2UgXCJuYXZpZ2F0aW5nVG9cIiBldmVudCBhdHRhY2hlZCBpbiBtYWluLXBhZ2UueG1sXHJcbmV4cG9ydCBmdW5jdGlvbiBuYXZpZ2F0aW5nVG8oYXJnczogRXZlbnREYXRhKSB7XHJcbiAgICAvKlxyXG4gICAgVGhpcyBnZXRzIGEgcmVmZXJlbmNlIHRoaXMgcGFnZeKAmXMgPFBhZ2U+IFVJIGNvbXBvbmVudC4gWW91IGNhblxyXG4gICAgdmlldyB0aGUgQVBJIHJlZmVyZW5jZSBvZiB0aGUgUGFnZSB0byBzZWUgd2hhdOKAmXMgYXZhaWxhYmxlIGF0XHJcbiAgICBodHRwczovL2RvY3MubmF0aXZlc2NyaXB0Lm9yZy9hcGktcmVmZXJlbmNlL2NsYXNzZXMvX3VpX3BhZ2VfLnBhZ2UuaHRtbFxyXG4gICAgKi9cclxuICAgIGxldCBwYWdlID0gPFBhZ2U+YXJncy5vYmplY3Q7XHJcblxyXG4gICAgXHJcbiAgICAvKlxyXG4gICAgQSBwYWdl4oCZcyBiaW5kaW5nQ29udGV4dCBpcyBhbiBvYmplY3QgdGhhdCBzaG91bGQgYmUgdXNlZCB0byBwZXJmb3JtXHJcbiAgICBkYXRhIGJpbmRpbmcgYmV0d2VlbiBYTUwgbWFya3VwIGFuZCBUeXBlU2NyaXB0IGNvZGUuIFByb3BlcnRpZXNcclxuICAgIG9uIHRoZSBiaW5kaW5nQ29udGV4dCBjYW4gYmUgYWNjZXNzZWQgdXNpbmcgdGhlIHt7IH19IHN5bnRheCBpbiBYTUwuXHJcbiAgICBJbiB0aGlzIGV4YW1wbGUsIHRoZSB7eyBtZXNzYWdlIH19IGFuZCB7eyBvblRhcCB9fSBiaW5kaW5ncyBhcmUgcmVzb2x2ZWRcclxuICAgIGFnYWluc3QgdGhlIG9iamVjdCByZXR1cm5lZCBieSBjcmVhdGVWaWV3TW9kZWwoKS5cclxuXHJcbiAgICBZb3UgY2FuIGxlYXJuIG1vcmUgYWJvdXQgZGF0YSBiaW5kaW5nIGluIE5hdGl2ZVNjcmlwdCBhdFxyXG4gICAgaHR0cHM6Ly9kb2NzLm5hdGl2ZXNjcmlwdC5vcmcvY29yZS1jb25jZXB0cy9kYXRhLWJpbmRpbmcuXHJcbiAgICAqL1xyXG4gICAgZmluZU1vZGVsID0gbmV3IEZpbmVQYWdlTW9kZWwoKTtcclxuXHJcbiAgICBwYWdlLmJpbmRpbmdDb250ZXh0ID0gZmluZU1vZGVsO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gb25JbnNlcnRGaW5lKGFyZ3M6YW55KSB7XHJcbiAgICAvLyBsZXQgdGV4dCA9IC5nZXRFbGVtZW50QnlJZChcInRmXCIpLmlubmVyVGV4dDtcclxuICAgIC8vIGNvbnNvbGUubG9nKHRleHQpO1xyXG4gICAgLy8gbGV0IGluZGV4ID0gYXJncy5pbmRleDsgXHJcbiAgICAvLyBjb25zb2xlLmxvZyhcImluZGV4IDo6IFwiK2luZGV4KTtcclxuICAgIC8vIGxldCBvYmogPSBmaW5lTW9kZWwubmFtZUxpc3QuZ2V0SXRlbShpbmRleCk7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhcIlBlcnNvbiBOYW1lIDo6IFwiK29iai5wZXJzb25OYW1lKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKFwiUGVyc29uIElkIDo6IFwiK29iai5wZXJzb25JZCk7XHJcbiAgICAvL2xldCBlbnRyeTpmcmFtZS5OYXZpZ2F0aW9uRW50cnkgPSB7bW9kdWxlTmFtZTondmlld3MvZGV0YWlscGFnZS9kZXRhaWxwYWdlJywgY29udGV4dDpvYmp9O1xyXG4gICAgLy9mcmFtZS50b3Btb3N0KCkubmF2aWdhdGUoZW50cnkpO1xyXG4gICAgLy8gY29uc29sZS5sb2coJ1JlZGlyZWN0Jyk7XHJcbiAgICAvLyBmcmFtZS50b3Btb3N0KCkubmF2aWdhdGUoXCJ2aWV3cy9maW5lcGFnZS9maW5lcGFnZVwiKTsgXHJcbn0iXX0=