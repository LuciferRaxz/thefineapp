"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var HttpUtility = (function () {
    function HttpUtility() {
    }
    HttpUtility.prototype.getMethod = function (query, accessToken) {
        return http.request({
            url: "https://concretio-dev-ed.my.salesforce.com/services/data/v20.0/query/?q=" + query,
            method: "GET",
            headers: {
                'Authorization': 'Bearer ' + accessToken
            }
        });
    };
    HttpUtility.prototype.postMethod = function (body) {
        return http.request({
            url: "https://login.salesforce.com/services/oauth2/token",
            method: "POST",
            content: encodeURI(body)
        });
    };
    return HttpUtility;
}());
exports.HttpUtility = HttpUtility;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSHR0cFV0aWxpdHkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJIdHRwVXRpbGl0eS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDJCQUE4QjtBQUU5QjtJQUFBO0lBbUJBLENBQUM7SUFsQlUsK0JBQVMsR0FBaEIsVUFBaUIsS0FBYyxFQUFFLFdBQW9CO1FBQ2pELE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ2hCLEdBQUcsRUFBRSwwRUFBMEUsR0FBQyxLQUFLO1lBQ3JGLE1BQU0sRUFBRSxLQUFLO1lBQ2IsT0FBTyxFQUNIO2dCQUNJLGVBQWUsRUFBQyxTQUFTLEdBQUMsV0FBVzthQUN4QztTQUNKLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSxnQ0FBVSxHQUFqQixVQUFrQixJQUFhO1FBQzNCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ2hCLEdBQUcsRUFBRSxvREFBb0Q7WUFDekQsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQztTQUMzQixDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0wsa0JBQUM7QUFBRCxDQUFDLEFBbkJELElBbUJDO0FBbkJZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGh0dHAgPSByZXF1aXJlKCdodHRwJyk7XHJcblxyXG5leHBvcnQgY2xhc3MgSHR0cFV0aWxpdHl7XHJcbiAgICBwdWJsaWMgZ2V0TWV0aG9kKHF1ZXJ5IDogc3RyaW5nLCBhY2Nlc3NUb2tlbiA6IFN0cmluZykgOiBQcm9taXNlPGh0dHAuSHR0cFJlc3BvbnNlPntcclxuICAgICAgICByZXR1cm4gaHR0cC5yZXF1ZXN0KHsgXHJcbiAgICAgICAgICAgIHVybDogXCJodHRwczovL2NvbmNyZXRpby1kZXYtZWQubXkuc2FsZXNmb3JjZS5jb20vc2VydmljZXMvZGF0YS92MjAuMC9xdWVyeS8/cT1cIitxdWVyeSxcclxuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiICxcclxuICAgICAgICAgICAgaGVhZGVyczpcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAnQXV0aG9yaXphdGlvbic6J0JlYXJlciAnK2FjY2Vzc1Rva2VuXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG4gXHJcbiAgICBwdWJsaWMgcG9zdE1ldGhvZChib2R5IDogc3RyaW5nKSA6IFByb21pc2U8aHR0cC5IdHRwUmVzcG9uc2U+e1xyXG4gICAgICAgIHJldHVybiBodHRwLnJlcXVlc3QoeyBcclxuICAgICAgICAgICAgdXJsOiBcImh0dHBzOi8vbG9naW4uc2FsZXNmb3JjZS5jb20vc2VydmljZXMvb2F1dGgyL3Rva2VuXCIsXHJcbiAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIgLFxyXG4gICAgICAgICAgICBjb250ZW50OiBlbmNvZGVVUkkoYm9keSlcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufSJdfQ==