"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var observable_array_1 = require("data/observable-array");
var FinePageServices_1 = require("../../FineAppServices/FinePageServices");
var fPServices;
var FinePageModel = (function (_super) {
    __extends(FinePageModel, _super);
    function FinePageModel() {
        var _this = _super.call(this) || this;
        _this._nameList = new observable_array_1.ObservableArray();
        fPServices = new FinePageServices_1.FinePageServices();
        _this.getListOfPersons();
        return _this;
    }
    Object.defineProperty(FinePageModel.prototype, "nameList", {
        get: function () {
            return this._nameList;
        },
        set: function (value) {
            this._nameList = value;
            this.notifyPropertyChange('nameList', value);
        },
        enumerable: true,
        configurable: true
    });
    FinePageModel.prototype.getListOfPersons = function () {
        var _this = this;
        fPServices.fetchNameList().then(function (response) {
            console.log(response.statusCode);
            if (response.statusCode != 200) {
                fPServices.getAccessToken();
                _this.getListOfPersons();
            }
            _this.nameList = fPServices.createDetailList(response.content.toJSON().records);
        });
    };
    return FinePageModel;
}(observable_1.Observable));
exports.FinePageModel = FinePageModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRmluZVBhZ2VNb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkZpbmVQYWdlTW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw4Q0FBc0Q7QUFDdEQsMERBQXNEO0FBQ3RELDJFQUEwRTtBQUcxRSxJQUFJLFVBQTZCLENBQUM7QUFHbEM7SUFBbUMsaUNBQVU7SUFHekM7UUFBQSxZQUNJLGlCQUFPLFNBR1Y7UUFOTyxlQUFTLEdBQTBCLElBQUksa0NBQWUsRUFBTyxDQUFDO1FBSWxFLFVBQVUsR0FBRyxJQUFJLG1DQUFnQixFQUFFLENBQUM7UUFDcEMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7O0lBQzVCLENBQUM7SUFFRCxzQkFBSSxtQ0FBUTthQUFaO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDMUIsQ0FBQzthQUNELFVBQWMsS0FBNEI7WUFDdEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNqRCxDQUFDOzs7T0FKQTtJQU1PLHdDQUFnQixHQUF4QjtRQUFBLGlCQVVDO1FBVEcsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVE7WUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDakMsRUFBRSxDQUFBLENBQUUsUUFBUSxDQUFDLFVBQVUsSUFBSSxHQUFHLENBQUMsQ0FBQSxDQUFDO2dCQUM1QixVQUFVLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQzVCLENBQUM7WUFDRCxLQUFJLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQUNMLG9CQUFDO0FBQUQsQ0FBQyxBQTVCRCxDQUFtQyx1QkFBVSxHQTRCNUM7QUE1Qlksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge09ic2VydmFibGUsZnJvbU9iamVjdH0gZnJvbSAnZGF0YS9vYnNlcnZhYmxlJztcclxuaW1wb3J0IHtPYnNlcnZhYmxlQXJyYXl9IGZyb20gJ2RhdGEvb2JzZXJ2YWJsZS1hcnJheSc7XHJcbmltcG9ydCB7IEZpbmVQYWdlU2VydmljZXMgfSBmcm9tICcuLi8uLi9GaW5lQXBwU2VydmljZXMvRmluZVBhZ2VTZXJ2aWNlcyc7XHJcbmltcG9ydCBodHRwID0gcmVxdWlyZSgnaHR0cCcpO1xyXG5cclxubGV0IGZQU2VydmljZXMgOiBGaW5lUGFnZVNlcnZpY2VzO1xyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBGaW5lUGFnZU1vZGVsIGV4dGVuZHMgT2JzZXJ2YWJsZXtcclxuICAgIHByaXZhdGUgX25hbWVMaXN0IDogT2JzZXJ2YWJsZUFycmF5PGFueT4gPSBuZXcgT2JzZXJ2YWJsZUFycmF5PGFueT4oKTtcclxuICAgIFxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTsgXHJcbiAgICAgICAgZlBTZXJ2aWNlcyA9IG5ldyBGaW5lUGFnZVNlcnZpY2VzKCk7XHJcbiAgICAgICAgdGhpcy5nZXRMaXN0T2ZQZXJzb25zKCk7XHJcbiAgICB9XHJcbiBcclxuICAgIGdldCBuYW1lTGlzdCAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX25hbWVMaXN0O1xyXG4gICAgfVxyXG4gICAgc2V0IG5hbWVMaXN0ICh2YWx1ZTpPYnNlcnZhYmxlQXJyYXk8SU5hbWU+KSB7XHJcbiAgICAgICAgdGhpcy5fbmFtZUxpc3QgPSB2YWx1ZTsgXHJcbiAgICAgICAgdGhpcy5ub3RpZnlQcm9wZXJ0eUNoYW5nZSgnbmFtZUxpc3QnLCB2YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRMaXN0T2ZQZXJzb25zKCkge1xyXG4gICAgICAgIGZQU2VydmljZXMuZmV0Y2hOYW1lTGlzdCgpLnRoZW4oKHJlc3BvbnNlKT0+eyBcclxuICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2Uuc3RhdHVzQ29kZSk7XHJcbiAgICAgICAgICAgIGlmKCByZXNwb25zZS5zdGF0dXNDb2RlICE9IDIwMCl7XHJcbiAgICAgICAgICAgICAgICBmUFNlcnZpY2VzLmdldEFjY2Vzc1Rva2VuKCk7IFxyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRMaXN0T2ZQZXJzb25zKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5uYW1lTGlzdCA9IGZQU2VydmljZXMuY3JlYXRlRGV0YWlsTGlzdChyZXNwb25zZS5jb250ZW50LnRvSlNPTigpLnJlY29yZHMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG59Il19