import {Observable} from 'data/observable';
import {ObservableArray} from 'data/observable-array';
import { ListPageServices } from '../../FineAppServices/ListPageServices';
import http = require('http');

let lPService : ListPageServices;
let lPModel : ListPageModel;

export class ListPageModel extends Observable{
    private _personList : ObservableArray<IPerson> = new ObservableArray<IPerson>();
    
    constructor() {
        super();
        lPService = new ListPageServices();
        this.getListOfPersons();
    }

    get personList () {
        return this._personList;
    }
    set personList (value:ObservableArray<IPerson>) {
        this._personList = value;
        this.notifyPropertyChange('personList', value);
    }

    private getListOfPersons() {
        
        lPService.fetchListOfPersons().then((response)=>{
            if( response.statusCode != 200){
                lPService.getAccessToken(); 
                this.getListOfPersons();
            }
            this.personList = lPService.createListOfPersons(response.content.toJSON().records);
        });
        
    }
}