"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var observable_array_1 = require("data/observable-array");
var http = require("http");
var AppConstantValues_1 = require("./FineAppServices/AppConstantValues");
var MainViewModel = (function (_super) {
    __extends(MainViewModel, _super);
    // private clientId:string= '3MVG9ZL0ppGP5UrAMylsPkSz0mAWH_jNbXoFekogJEXhQhnt2ZYqcAOn1Jkzzn1qSvV13G3YFd38FiET0FXF4';
    // private clientSecret:string = '8536980590341800865';
    // private userName: string  = 'tarique@trailhead.com';
    // private password: string ='$raxz@lucifer769FYMZSPM04PskBmwMY7DMQp2IA';
    // private accessToken : string = '00D28000001iAuy!AQsAQMixg5lf3nBOS1XnEtJMdemBk7NtQbjOT3bH3YCRzAX6aSUDF.TUzpgmyMv7jhofPMEK43r2vtamSvpiGi9dUBSp5An_';
    function MainViewModel() {
        var _this = _super.call(this) || this;
        _this._personList = new observable_array_1.ObservableArray();
        return _this;
        //this.getListOfPersons();
    }
    Object.defineProperty(MainViewModel.prototype, "personList", {
        get: function () {
            return this._personList;
        },
        set: function (value) {
            this._personList = value;
            this.notifyPropertyChange('personList', value);
        },
        enumerable: true,
        configurable: true
    });
    MainViewModel.prototype.getListOfPersons = function () {
        var _this = this;
        console.log("getListOfPersons");
        //this.getAccessToken();
        http.request({
            url: "https://concretio-dev-ed.my.salesforce.com/services/data/v20.0/query/?q=SELECT+id,name,Total_Fine__c+from+Person__c+LIMIT+1",
            method: "GET",
            headers: {
                //'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AppConstantValues_1.AppConstantValues.ACCESS_TOKEN //this.accessToken
            }
        }).then(function (response) {
            console.log(response.statusCode);
            if (response.statusCode != 200) {
                _this.getAccessToken();
                return;
            }
            var fetchRecords = response.content.toJSON().records;
            _this.createListOfPersons(fetchRecords);
        }, function (error) {
            console.log('error' + error);
        });
    };
    MainViewModel.prototype.createListOfPersons = function (records) {
        console.log('dgsg');
        console.log(records);
        for (var idx = 0; idx < records.length; idx++) {
            var singlePerson = { personId: '', personTotalFine: 0, personName: '' };
            singlePerson.personId = records[idx].Id;
            singlePerson.personName = records[idx].Name;
            singlePerson.personTotalFine = records[idx].Total_Fine__c;
            this._personList.push(singlePerson);
        }
    };
    MainViewModel.prototype.getAccessToken = function () {
        //let req:http.HttpRequestOptions = {}
        var body = "grant_type=password&client_id=" + AppConstantValues_1.AppConstantValues.CLIENT_ID + "&client_secret=" + AppConstantValues_1.AppConstantValues.CLIENT_SECRET + "&username=" + AppConstantValues_1.AppConstantValues.USER_NAME + "&password=" + AppConstantValues_1.AppConstantValues.PASSWORD;
        http.request({
            url: "https://login.salesforce.com/services/oauth2/token",
            method: "POST",
            content: encodeURI(body)
        }).then(function (response) {
            this.accessToken = response.content.toJSON().access_token;
            this.makeCall();
        }, function (e) {
            //// Argument (e) is Error!
        });
    };
    return MainViewModel;
}(observable_1.Observable));
exports.MainViewModel = MainViewModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi12aWV3LW1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibWFpbi12aWV3LW1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsOENBQTJDO0FBQzNDLDBEQUFzRDtBQUN0RCwyQkFBOEI7QUFDOUIseUVBQXNFO0FBRXRFO0lBQW1DLGlDQUFVO0lBSXpDLG9IQUFvSDtJQUNwSCx1REFBdUQ7SUFDdkQsdURBQXVEO0lBQ3ZELHlFQUF5RTtJQUN6RSxxSkFBcUo7SUFDcko7UUFBQSxZQUNJLGlCQUFPLFNBR1Y7UUFaTyxpQkFBVyxHQUE4QixJQUFJLGtDQUFlLEVBQVcsQ0FBQzs7UUFXNUUsMEJBQTBCO0lBQzlCLENBQUM7SUFFRCxzQkFBSSxxQ0FBVTthQUFkO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDNUIsQ0FBQzthQUNELFVBQWdCLEtBQThCO1lBQzFDLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbkQsQ0FBQzs7O09BSkE7SUFNTyx3Q0FBZ0IsR0FBeEI7UUFBQSxpQkFzQkM7UUFyQkcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2hDLHdCQUF3QjtRQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1QsR0FBRyxFQUFFLDZIQUE2SDtZQUNsSSxNQUFNLEVBQUUsS0FBSztZQUNiLE9BQU8sRUFDSDtnQkFDSSxxQ0FBcUM7Z0JBQ3JDLGFBQWEsRUFBQyxTQUFTLEdBQUMscUNBQWlCLENBQUMsWUFBWSxDQUFBLGtCQUFrQjthQUMzRTtTQUNKLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRO1lBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDakMsRUFBRSxDQUFBLENBQUUsUUFBUSxDQUFDLFVBQVUsSUFBSSxHQUFHLENBQUMsQ0FBQSxDQUFDO2dCQUM1QixLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ3RCLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFDRCxJQUFJLFlBQVksR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sQ0FBQztZQUNyRCxLQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDM0MsQ0FBQyxFQUFFLFVBQUMsS0FBSztZQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVPLDJDQUFtQixHQUEzQixVQUE0QixPQUFhO1FBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyQixHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLEVBQUUsR0FBRyxHQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUcsR0FBRyxFQUFFLEVBQUMsQ0FBQztZQUN6QyxJQUFJLFlBQVksR0FBWSxFQUFDLFFBQVEsRUFBRSxFQUFFLEVBQUUsZUFBZSxFQUFDLENBQUMsRUFBRSxVQUFVLEVBQUMsRUFBRSxFQUFDLENBQUM7WUFDN0UsWUFBWSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3hDLFlBQVksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUM1QyxZQUFZLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7WUFDMUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEMsQ0FBQztJQUVMLENBQUM7SUFDTyxzQ0FBYyxHQUF0QjtRQUNJLHNDQUFzQztRQUN0QyxJQUFJLElBQUksR0FBVSxtQ0FBaUMscUNBQWlCLENBQUMsU0FBUyx1QkFBa0IscUNBQWlCLENBQUMsYUFBYSxrQkFBYSxxQ0FBaUIsQ0FBQyxTQUFTLGtCQUFhLHFDQUFpQixDQUFDLFFBQVUsQ0FBQztRQUNqTixJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1QsR0FBRyxFQUFFLG9EQUFvRDtZQUN6RCxNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDO1NBQzNCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxRQUFRO1lBQ2xCLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxZQUFZLENBQUM7WUFDMUQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hCLENBQUMsRUFBRSxVQUFVLENBQUM7WUFDViwyQkFBMkI7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUwsb0JBQUM7QUFBRCxDQUFDLEFBMUVELENBQW1DLHVCQUFVLEdBMEU1QztBQTFFWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAnZGF0YS9vYnNlcnZhYmxlJztcbmltcG9ydCB7T2JzZXJ2YWJsZUFycmF5fSBmcm9tICdkYXRhL29ic2VydmFibGUtYXJyYXknO1xuaW1wb3J0IGh0dHAgPSByZXF1aXJlKCdodHRwJyk7XG5pbXBvcnQge0FwcENvbnN0YW50VmFsdWVzfSBmcm9tICcuL0ZpbmVBcHBTZXJ2aWNlcy9BcHBDb25zdGFudFZhbHVlcyc7XG5cbmV4cG9ydCBjbGFzcyBNYWluVmlld01vZGVsIGV4dGVuZHMgT2JzZXJ2YWJsZSB7XG4gICAgcHJpdmF0ZSBfcGVyc29uTGlzdCA6IE9ic2VydmFibGVBcnJheTxJUGVyc29uPiA9IG5ldyBPYnNlcnZhYmxlQXJyYXk8SVBlcnNvbj4oKTtcbiAgICBwcml2YXRlIF9jb3VudGVyOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBfbWVzc2FnZTogc3RyaW5nO1xuICAgIC8vIHByaXZhdGUgY2xpZW50SWQ6c3RyaW5nPSAnM01WRzlaTDBwcEdQNVVyQU15bHNQa1N6MG1BV0hfak5iWG9GZWtvZ0pFWGhRaG50MlpZcWNBT24xSmt6em4xcVN2VjEzRzNZRmQzOEZpRVQwRlhGNCc7XG4gICAgLy8gcHJpdmF0ZSBjbGllbnRTZWNyZXQ6c3RyaW5nID0gJzg1MzY5ODA1OTAzNDE4MDA4NjUnO1xuICAgIC8vIHByaXZhdGUgdXNlck5hbWU6IHN0cmluZyAgPSAndGFyaXF1ZUB0cmFpbGhlYWQuY29tJztcbiAgICAvLyBwcml2YXRlIHBhc3N3b3JkOiBzdHJpbmcgPSckcmF4ekBsdWNpZmVyNzY5RllNWlNQTTA0UHNrQm13TVk3RE1RcDJJQSc7XG4gICAgLy8gcHJpdmF0ZSBhY2Nlc3NUb2tlbiA6IHN0cmluZyA9ICcwMEQyODAwMDAwMWlBdXkhQVFzQVFNaXhnNWxmM25CT1MxWG5FdEpNZGVtQms3TnRRYmpPVDNiSDNZQ1J6QVg2YVNVREYuVFV6cGdteU12N2pob2ZQTUVLNDNyMnZ0YW1TdnBpR2k5ZFVCU3A1QW5fJztcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy5nZXRMaXN0T2ZQZXJzb25zKCk7XG4gICAgfVxuXG4gICAgZ2V0IHBlcnNvbkxpc3QgKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fcGVyc29uTGlzdDtcbiAgICB9XG4gICAgc2V0IHBlcnNvbkxpc3QgKHZhbHVlOk9ic2VydmFibGVBcnJheTxJUGVyc29uPikge1xuICAgICAgICB0aGlzLl9wZXJzb25MaXN0ID0gdmFsdWU7XG4gICAgICAgIHRoaXMubm90aWZ5UHJvcGVydHlDaGFuZ2UoJ3BlcnNvbkxpc3QnLCB2YWx1ZSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRMaXN0T2ZQZXJzb25zKCkge1xuICAgICAgICBjb25zb2xlLmxvZyhcImdldExpc3RPZlBlcnNvbnNcIik7XG4gICAgICAgIC8vdGhpcy5nZXRBY2Nlc3NUb2tlbigpO1xuICAgICAgICBodHRwLnJlcXVlc3QoeyBcbiAgICAgICAgICAgIHVybDogXCJodHRwczovL2NvbmNyZXRpby1kZXYtZWQubXkuc2FsZXNmb3JjZS5jb20vc2VydmljZXMvZGF0YS92MjAuMC9xdWVyeS8/cT1TRUxFQ1QraWQsbmFtZSxUb3RhbF9GaW5lX19jK2Zyb20rUGVyc29uX19jK0xJTUlUKzFcIixcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIiAsXG4gICAgICAgICAgICBoZWFkZXJzOlxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgLy8nQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAgICAgICAgICAgICBBdXRob3JpemF0aW9uOidCZWFyZXIgJytBcHBDb25zdGFudFZhbHVlcy5BQ0NFU1NfVE9LRU4vL3RoaXMuYWNjZXNzVG9rZW5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlLnN0YXR1c0NvZGUpO1xuICAgICAgICAgICAgICAgIGlmKCByZXNwb25zZS5zdGF0dXNDb2RlICE9IDIwMCl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0QWNjZXNzVG9rZW4oKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB2YXIgZmV0Y2hSZWNvcmRzID0gcmVzcG9uc2UuY29udGVudC50b0pTT04oKS5yZWNvcmRzO1xuICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlTGlzdE9mUGVyc29ucyhmZXRjaFJlY29yZHMpO1xuICAgICAgICAgICAgfSwgKGVycm9yKT0+IHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZXJyb3InK2Vycm9yKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cbiBcbiAgICBwcml2YXRlIGNyZWF0ZUxpc3RPZlBlcnNvbnMocmVjb3JkcyA6IGFueSl7XG4gICAgICAgIGNvbnNvbGUubG9nKCdkZ3NnJyk7XG4gICAgICAgIGNvbnNvbGUubG9nKHJlY29yZHMpO1xuICAgICAgICBmb3IodmFyIGlkeCA9IDA7IGlkeDxyZWNvcmRzLmxlbmd0aCA7IGlkeCsrKXsgXG4gICAgICAgICAgICB2YXIgc2luZ2xlUGVyc29uIDpJUGVyc29uID0ge3BlcnNvbklkOiAnJywgcGVyc29uVG90YWxGaW5lOjAsIHBlcnNvbk5hbWU6Jyd9O1xuICAgICAgICAgICAgc2luZ2xlUGVyc29uLnBlcnNvbklkID0gcmVjb3Jkc1tpZHhdLklkO1xuICAgICAgICAgICAgc2luZ2xlUGVyc29uLnBlcnNvbk5hbWUgPSByZWNvcmRzW2lkeF0uTmFtZTtcbiAgICAgICAgICAgIHNpbmdsZVBlcnNvbi5wZXJzb25Ub3RhbEZpbmUgPSByZWNvcmRzW2lkeF0uVG90YWxfRmluZV9fYztcbiAgICAgICAgICAgIHRoaXMuX3BlcnNvbkxpc3QucHVzaChzaW5nbGVQZXJzb24pO1xuICAgICAgICB9ICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIHByaXZhdGUgZ2V0QWNjZXNzVG9rZW4oKXtcbiAgICAgICAgLy9sZXQgcmVxOmh0dHAuSHR0cFJlcXVlc3RPcHRpb25zID0ge31cbiAgICAgICAgdmFyIGJvZHk6c3RyaW5nID0gYGdyYW50X3R5cGU9cGFzc3dvcmQmY2xpZW50X2lkPSR7QXBwQ29uc3RhbnRWYWx1ZXMuQ0xJRU5UX0lEfSZjbGllbnRfc2VjcmV0PSR7QXBwQ29uc3RhbnRWYWx1ZXMuQ0xJRU5UX1NFQ1JFVH0mdXNlcm5hbWU9JHtBcHBDb25zdGFudFZhbHVlcy5VU0VSX05BTUV9JnBhc3N3b3JkPSR7QXBwQ29uc3RhbnRWYWx1ZXMuUEFTU1dPUkR9YDtcbiAgICAgICAgaHR0cC5yZXF1ZXN0KHsgXG4gICAgICAgICAgICB1cmw6IFwiaHR0cHM6Ly9sb2dpbi5zYWxlc2ZvcmNlLmNvbS9zZXJ2aWNlcy9vYXV0aDIvdG9rZW5cIixcbiAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIgLFxuICAgICAgICAgICAgY29udGVudDogZW5jb2RlVVJJKGJvZHkpXG4gICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5hY2Nlc3NUb2tlbiA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCkuYWNjZXNzX3Rva2VuO1xuICAgICAgICAgICAgICAgIHRoaXMubWFrZUNhbGwoKTtcbiAgICAgICAgfSwgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIC8vLy8gQXJndW1lbnQgKGUpIGlzIEVycm9yIVxuICAgICAgICB9KTtcbiAgICB9XG4gICAgXG59Il19