import {Observable,fromObject} from 'data/observable';
import {ObservableArray} from 'data/observable-array';
import { FinePageServices } from '../../FineAppServices/FinePageServices';
import http = require('http');

let fPServices : FinePageServices;


export class FinePageModel extends Observable{
    private _nameList : ObservableArray<any> = new ObservableArray<any>();
    
    constructor() {
        super(); 
        fPServices = new FinePageServices();
        this.getListOfPersons();
    }
 
    get nameList () {
        return this._nameList;
    }
    set nameList (value:ObservableArray<IName>) {
        this._nameList = value; 
        this.notifyPropertyChange('nameList', value);
    }

    private getListOfPersons() {
        fPServices.fetchNameList().then((response)=>{ 
            console.log(response.statusCode);
            if( response.statusCode != 200){
                fPServices.getAccessToken(); 
                this.getListOfPersons();
            }
            this.nameList = fPServices.createDetailList(response.content.toJSON().records);
        });
        
    }
}