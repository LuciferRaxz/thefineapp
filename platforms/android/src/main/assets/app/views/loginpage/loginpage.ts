/*
Login page event handler here to handle the action from login form page
*/

import { EventData } from 'data/observable';
import { Page } from 'ui/page';
import { MainViewModel } from '../../main-view-model';
import * as frame from "ui/frame";

let viewModel:MainViewModel; 

// Event handler for Page "navigatingTo" event attached in main-page.xml
export function navigatingTo(args: EventData) {
    /*
    This gets a reference this page’s <Page> UI component. You can
    view the API reference of the Page to see what’s available at
    https://docs.nativescript.org/api-reference/classes/_ui_page_.page.html
    */
    let page = <Page>args.object;
    
    /*
    A page’s bindingContext is an object that should be used to perform
    data binding between XML markup and TypeScript code. Properties
    on the bindingContext can be accessed using the {{ }} syntax in XML.
    In this example, the {{ message }} and {{ onTap }} bindings are resolved
    against the object returned by createViewModel().

    You can learn more about data binding in NativeScript at
    https://docs.nativescript.org/core-concepts/data-binding.
    */
    viewModel = new MainViewModel();

    page.bindingContext = viewModel;
}

export function onLogin() {
    frame.topmost().navigate("views/choicepage/choicepage"); 
}
export function onSignUp(){
    frame.topmost().navigate("views/signuppage/signuppage");
}