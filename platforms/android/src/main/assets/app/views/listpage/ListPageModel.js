"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var observable_array_1 = require("data/observable-array");
var ListPageServices_1 = require("../../FineAppServices/ListPageServices");
var lPService;
var lPModel;
var ListPageModel = (function (_super) {
    __extends(ListPageModel, _super);
    function ListPageModel() {
        var _this = _super.call(this) || this;
        _this._personList = new observable_array_1.ObservableArray();
        lPService = new ListPageServices_1.ListPageServices();
        _this.getListOfPersons();
        return _this;
    }
    Object.defineProperty(ListPageModel.prototype, "personList", {
        get: function () {
            return this._personList;
        },
        set: function (value) {
            this._personList = value;
            this.notifyPropertyChange('personList', value);
        },
        enumerable: true,
        configurable: true
    });
    ListPageModel.prototype.getListOfPersons = function () {
        var _this = this;
        lPService.fetchListOfPersons().then(function (response) {
            if (response.statusCode != 200) {
                lPService.getAccessToken();
                _this.getListOfPersons();
            }
            _this.personList = lPService.createListOfPersons(response.content.toJSON().records);
        });
    };
    return ListPageModel;
}(observable_1.Observable));
exports.ListPageModel = ListPageModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGlzdFBhZ2VNb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkxpc3RQYWdlTW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw4Q0FBMkM7QUFDM0MsMERBQXNEO0FBQ3RELDJFQUEwRTtBQUcxRSxJQUFJLFNBQTRCLENBQUM7QUFDakMsSUFBSSxPQUF1QixDQUFDO0FBRTVCO0lBQW1DLGlDQUFVO0lBR3pDO1FBQUEsWUFDSSxpQkFBTyxTQUdWO1FBTk8saUJBQVcsR0FBOEIsSUFBSSxrQ0FBZSxFQUFXLENBQUM7UUFJNUUsU0FBUyxHQUFHLElBQUksbUNBQWdCLEVBQUUsQ0FBQztRQUNuQyxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzs7SUFDNUIsQ0FBQztJQUVELHNCQUFJLHFDQUFVO2FBQWQ7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUM1QixDQUFDO2FBQ0QsVUFBZ0IsS0FBOEI7WUFDMUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7WUFDekIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNuRCxDQUFDOzs7T0FKQTtJQU1PLHdDQUFnQixHQUF4QjtRQUFBLGlCQVVDO1FBUkcsU0FBUyxDQUFDLGtCQUFrQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBUTtZQUN6QyxFQUFFLENBQUEsQ0FBRSxRQUFRLENBQUMsVUFBVSxJQUFJLEdBQUcsQ0FBQyxDQUFBLENBQUM7Z0JBQzVCLFNBQVMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDM0IsS0FBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDNUIsQ0FBQztZQUNELEtBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkYsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQUFDLEFBNUJELENBQW1DLHVCQUFVLEdBNEI1QztBQTVCWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAnZGF0YS9vYnNlcnZhYmxlJztcclxuaW1wb3J0IHtPYnNlcnZhYmxlQXJyYXl9IGZyb20gJ2RhdGEvb2JzZXJ2YWJsZS1hcnJheSc7XHJcbmltcG9ydCB7IExpc3RQYWdlU2VydmljZXMgfSBmcm9tICcuLi8uLi9GaW5lQXBwU2VydmljZXMvTGlzdFBhZ2VTZXJ2aWNlcyc7XHJcbmltcG9ydCBodHRwID0gcmVxdWlyZSgnaHR0cCcpO1xyXG5cclxubGV0IGxQU2VydmljZSA6IExpc3RQYWdlU2VydmljZXM7XHJcbmxldCBsUE1vZGVsIDogTGlzdFBhZ2VNb2RlbDtcclxuXHJcbmV4cG9ydCBjbGFzcyBMaXN0UGFnZU1vZGVsIGV4dGVuZHMgT2JzZXJ2YWJsZXtcclxuICAgIHByaXZhdGUgX3BlcnNvbkxpc3QgOiBPYnNlcnZhYmxlQXJyYXk8SVBlcnNvbj4gPSBuZXcgT2JzZXJ2YWJsZUFycmF5PElQZXJzb24+KCk7XHJcbiAgICBcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgbFBTZXJ2aWNlID0gbmV3IExpc3RQYWdlU2VydmljZXMoKTtcclxuICAgICAgICB0aGlzLmdldExpc3RPZlBlcnNvbnMoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcGVyc29uTGlzdCAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3BlcnNvbkxpc3Q7XHJcbiAgICB9XHJcbiAgICBzZXQgcGVyc29uTGlzdCAodmFsdWU6T2JzZXJ2YWJsZUFycmF5PElQZXJzb24+KSB7XHJcbiAgICAgICAgdGhpcy5fcGVyc29uTGlzdCA9IHZhbHVlO1xyXG4gICAgICAgIHRoaXMubm90aWZ5UHJvcGVydHlDaGFuZ2UoJ3BlcnNvbkxpc3QnLCB2YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRMaXN0T2ZQZXJzb25zKCkge1xyXG4gICAgICAgIFxyXG4gICAgICAgIGxQU2VydmljZS5mZXRjaExpc3RPZlBlcnNvbnMoKS50aGVuKChyZXNwb25zZSk9PntcclxuICAgICAgICAgICAgaWYoIHJlc3BvbnNlLnN0YXR1c0NvZGUgIT0gMjAwKXtcclxuICAgICAgICAgICAgICAgIGxQU2VydmljZS5nZXRBY2Nlc3NUb2tlbigpOyBcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0TGlzdE9mUGVyc29ucygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMucGVyc29uTGlzdCA9IGxQU2VydmljZS5jcmVhdGVMaXN0T2ZQZXJzb25zKHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCkucmVjb3Jkcyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbn0iXX0=