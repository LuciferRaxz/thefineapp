"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var observable_array_1 = require("data/observable-array");
var HttpUtility_1 = require("../HttpUtility/HttpUtility");
var AppConstantValues_1 = require("./AppConstantValues");
var httpUtility;
var ListPageServices = (function (_super) {
    __extends(ListPageServices, _super);
    function ListPageServices() {
        var _this = _super.call(this) || this;
        httpUtility = new HttpUtility_1.HttpUtility();
        return _this;
    }
    Object.defineProperty(ListPageServices.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (value) {
            this._data = value;
            this.notifyPropertyChange('data', value);
        },
        enumerable: true,
        configurable: true
    });
    ListPageServices.prototype.fetchListOfPersons = function () {
        var personList = new observable_array_1.ObservableArray();
        var query = "SELECT+id,name,Total_Fine__c+from+Person__c";
        return httpUtility.getMethod(query, AppConstantValues_1.AppConstantValues.ACCESS_TOKEN);
    };
    ListPageServices.prototype.createListOfPersons = function (records) {
        var personList = new observable_array_1.ObservableArray();
        for (var idx = 0; idx < records.length; idx++) {
            var singlePerson = { personId: '', personTotalFine: 0, personName: '' };
            singlePerson.personId = records[idx].Id;
            singlePerson.personName = records[idx].Name;
            singlePerson.personTotalFine = records[idx].Total_Fine__c;
            personList.push(singlePerson);
        }
        return personList;
    };
    ListPageServices.prototype.getAccessToken = function () {
        var _this = this;
        var body = AppConstantValues_1.AppConstantValues.ACCESS_TOKEN_BODY;
        var postResponse = httpUtility.postMethod(body);
        postResponse.then(function (response) {
            AppConstantValues_1.AppConstantValues.ACCESS_TOKEN = response.content.toJSON().access_token;
            _this.fetchListOfPersons();
        }, function (error) {
            //// Argument (e) is Error!
            console.log(error);
        });
    };
    return ListPageServices;
}(observable_1.Observable));
exports.ListPageServices = ListPageServices;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGlzdFBhZ2VTZXJ2aWNlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkxpc3RQYWdlU2VydmljZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw4Q0FBa0U7QUFDbEUsMERBQXNEO0FBQ3RELDBEQUFzRDtBQUd0RCx5REFBc0Q7QUFJdEQsSUFBSSxXQUF5QixDQUFDO0FBRTlCO0lBQXNDLG9DQUFVO0lBUzVDO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBREcsV0FBVyxHQUFHLElBQUkseUJBQVcsRUFBRSxDQUFDOztJQUNwQyxDQUFDO0lBRUQsc0JBQUksa0NBQUk7YUFJUjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3RCLENBQUM7YUFORCxVQUFTLEtBQThCO1lBQ25DLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ25CLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDN0MsQ0FBQzs7O09BQUE7SUFLTSw2Q0FBa0IsR0FBekI7UUFDSSxJQUFJLFVBQVUsR0FBRyxJQUFJLGtDQUFlLEVBQVcsQ0FBQztRQUNoRCxJQUFJLEtBQUssR0FBRyw2Q0FBNkMsQ0FBQztRQUMxRCxNQUFNLENBQUUsV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUMscUNBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUVNLDhDQUFtQixHQUExQixVQUEyQixPQUFhO1FBQ3BDLElBQUksVUFBVSxHQUFHLElBQUksa0NBQWUsRUFBVyxDQUFDO1FBQ2hELEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRyxHQUFHLEVBQUUsRUFBQyxDQUFDO1lBQ3pDLElBQUksWUFBWSxHQUFZLEVBQUMsUUFBUSxFQUFFLEVBQUUsRUFBRSxlQUFlLEVBQUMsQ0FBQyxFQUFFLFVBQVUsRUFBQyxFQUFFLEVBQUMsQ0FBQztZQUM3RSxZQUFZLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDeEMsWUFBWSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzVDLFlBQVksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztZQUMxRCxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2xDLENBQUM7UUFDRCxNQUFNLENBQUMsVUFBVSxDQUFDO0lBQ3RCLENBQUM7SUFFTSx5Q0FBYyxHQUFyQjtRQUFBLGlCQVVDO1FBVEcsSUFBSSxJQUFJLEdBQVUscUNBQWlCLENBQUMsaUJBQWlCLENBQUM7UUFDdEQsSUFBSSxZQUFZLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoRCxZQUFZLENBQUMsSUFBSSxDQUFFLFVBQUMsUUFBUTtZQUNwQixxQ0FBaUIsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxZQUFZLENBQUM7WUFDeEUsS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDbEMsQ0FBQyxFQUFFLFVBQUMsS0FBSztZQUNMLDJCQUEyQjtZQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVMLHVCQUFDO0FBQUQsQ0FBQyxBQXBERCxDQUFzQyx1QkFBVSxHQW9EL0M7QUFwRFksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtPYnNlcnZhYmxlLCBmcm9tT2JqZWN0LCBFdmVudERhdGF9IGZyb20gJ2RhdGEvb2JzZXJ2YWJsZSc7XHJcbmltcG9ydCB7T2JzZXJ2YWJsZUFycmF5fSBmcm9tICdkYXRhL29ic2VydmFibGUtYXJyYXknO1xyXG5pbXBvcnQge0h0dHBVdGlsaXR5fSBmcm9tICcuLi9IdHRwVXRpbGl0eS9IdHRwVXRpbGl0eSdcclxuaW1wb3J0IGh0dHAgPSByZXF1aXJlKCdodHRwJyk7XHJcbmltcG9ydCB7TGlzdFBhZ2VNb2RlbH0gZnJvbSAnLi4vdmlld3MvbGlzdHBhZ2UvTGlzdFBhZ2VNb2RlbCc7XHJcbmltcG9ydCB7QXBwQ29uc3RhbnRWYWx1ZXN9IGZyb20gJy4vQXBwQ29uc3RhbnRWYWx1ZXMnO1xyXG5cclxuXHJcblxyXG5sZXQgaHR0cFV0aWxpdHkgOiBIdHRwVXRpbGl0eTtcclxuICBcclxuZXhwb3J0IGNsYXNzIExpc3RQYWdlU2VydmljZXMgZXh0ZW5kcyBPYnNlcnZhYmxlIHtcclxuXHJcbiAgICAvL05lY2Vzc2FyeSB2YXJpYWJsZXMgcmVxaXJlZCBmb3IgdGhlIEFjY2VzcyB0b2tlblxyXG4gICAgLy8gcHJpdmF0ZSBjbGllbnRJZDpzdHJpbmc9ICczTVZHOVpMMHBwR1A1VXJBTXlsc1BrU3owbUFXSF9qTmJYb0Zla29nSkVYaFFobnQyWllxY0FPbjFKa3p6bjFxU3ZWMTNHM1lGZDM4RmlFVDBGWEY0JztcclxuICAgIC8vIHByaXZhdGUgY2xpZW50U2VjcmV0OnN0cmluZyA9ICc4NTM2OTgwNTkwMzQxODAwODY1JztcclxuICAgIC8vIHByaXZhdGUgdXNlck5hbWU6IHN0cmluZyAgPSAndGFyaXF1ZUB0cmFpbGhlYWQuY29tJztcclxuICAgIC8vIHByaXZhdGUgcGFzc3dvcmQ6IHN0cmluZyA9JyRyYXh6QGx1Y2lmZXI3NjlGWU1aU1BNMDRQc2tCbXdNWTdETVFwMklBJztcclxuICAgIC8vIHByaXZhdGUgYWNjZXNzVG9rZW4gOiBzdHJpbmcgPSAnMDBEMjgwMDAwMDFpQXV5IUFRc0FRS095LllYbDFpdlpldXNyNlR4aVMyU1VtSlRwVENhQXlMT3cud0picTcuVmhoeHdPZG9DWUxDTXloU1VsMnpJZXVqdC5Jb3JGeUN0c0JTWHNETVNLbzU5SU1VNCc7XHJcbiAgICBwcml2YXRlIF9kYXRhOk9ic2VydmFibGVBcnJheTxJUGVyc29uPjtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgaHR0cFV0aWxpdHkgPSBuZXcgSHR0cFV0aWxpdHkoKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgZGF0YSh2YWx1ZTpPYnNlcnZhYmxlQXJyYXk8SVBlcnNvbj4pIHtcclxuICAgICAgICB0aGlzLl9kYXRhID0gdmFsdWU7XHJcbiAgICAgICAgdGhpcy5ub3RpZnlQcm9wZXJ0eUNoYW5nZSgnZGF0YScsIHZhbHVlKTtcclxuICAgIH1cclxuICAgIGdldCBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBmZXRjaExpc3RPZlBlcnNvbnMoKSA6IFByb21pc2U8aHR0cC5IdHRwUmVzcG9uc2U+e1xyXG4gICAgICAgIGxldCBwZXJzb25MaXN0ID0gbmV3IE9ic2VydmFibGVBcnJheTxJUGVyc29uPigpO1xyXG4gICAgICAgIHZhciBxdWVyeSA9IFwiU0VMRUNUK2lkLG5hbWUsVG90YWxfRmluZV9fYytmcm9tK1BlcnNvbl9fY1wiO1xyXG4gICAgICAgIHJldHVybiAgaHR0cFV0aWxpdHkuZ2V0TWV0aG9kKHF1ZXJ5LEFwcENvbnN0YW50VmFsdWVzLkFDQ0VTU19UT0tFTik7XHJcbiAgICB9XHJcbiBcclxuICAgIHB1YmxpYyBjcmVhdGVMaXN0T2ZQZXJzb25zKHJlY29yZHMgOiBhbnkpIDogT2JzZXJ2YWJsZUFycmF5PElQZXJzb24+e1xyXG4gICAgICAgIGxldCBwZXJzb25MaXN0ID0gbmV3IE9ic2VydmFibGVBcnJheTxJUGVyc29uPigpO1xyXG4gICAgICAgIGZvcih2YXIgaWR4ID0gMDsgaWR4PHJlY29yZHMubGVuZ3RoIDsgaWR4KyspeyBcclxuICAgICAgICAgICAgdmFyIHNpbmdsZVBlcnNvbiA6SVBlcnNvbiA9IHtwZXJzb25JZDogJycsIHBlcnNvblRvdGFsRmluZTowLCBwZXJzb25OYW1lOicnfTtcclxuICAgICAgICAgICAgc2luZ2xlUGVyc29uLnBlcnNvbklkID0gcmVjb3Jkc1tpZHhdLklkO1xyXG4gICAgICAgICAgICBzaW5nbGVQZXJzb24ucGVyc29uTmFtZSA9IHJlY29yZHNbaWR4XS5OYW1lO1xyXG4gICAgICAgICAgICBzaW5nbGVQZXJzb24ucGVyc29uVG90YWxGaW5lID0gcmVjb3Jkc1tpZHhdLlRvdGFsX0ZpbmVfX2M7XHJcbiAgICAgICAgICAgIHBlcnNvbkxpc3QucHVzaChzaW5nbGVQZXJzb24pO1xyXG4gICAgICAgIH0gICAgIFxyXG4gICAgICAgIHJldHVybiBwZXJzb25MaXN0O1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRBY2Nlc3NUb2tlbigpe1xyXG4gICAgICAgIHZhciBib2R5OnN0cmluZyA9IEFwcENvbnN0YW50VmFsdWVzLkFDQ0VTU19UT0tFTl9CT0RZO1xyXG4gICAgICAgIHZhciBwb3N0UmVzcG9uc2UgPSBodHRwVXRpbGl0eS5wb3N0TWV0aG9kKGJvZHkpO1xyXG4gICAgICAgIHBvc3RSZXNwb25zZS50aGVuKCAocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgIEFwcENvbnN0YW50VmFsdWVzLkFDQ0VTU19UT0tFTiA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCkuYWNjZXNzX3Rva2VuO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mZXRjaExpc3RPZlBlcnNvbnMoKTtcclxuICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgLy8vLyBBcmd1bWVudCAoZSkgaXMgRXJyb3IhXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIFxyXG59Il19