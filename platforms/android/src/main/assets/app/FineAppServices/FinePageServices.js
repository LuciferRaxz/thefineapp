"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var observable_array_1 = require("data/observable-array");
var HttpUtility_1 = require("../HttpUtility/HttpUtility");
var AppConstantValues_1 = require("./AppConstantValues");
var httpUtility;
var FinePageServices = (function (_super) {
    __extends(FinePageServices, _super);
    //Necessary variables reqired for the Access token
    // private clientId:string= '3MVG9ZL0ppGP5UrAMylsPkSz0mAWH_jNbXoFekogJEXhQhnt2ZYqcAOn1Jkzzn1qSvV13G3YFd38FiET0FXF4';
    // private clientSecret:string = '8536980590341800865';
    // private userName: string  = 'tarique@trailhead.com';
    // private password: string ='$raxz@lucifer769FYMZSPM04PskBmwMY7DMQp2IA';
    // private accessToken : string = '00D28000001iAuy!AQsAQKOy.YXl1ivZeusr6TxiS2SUmJTpTCaAyLOw.wJbq7.VhhxwOdoCYLCMyhSUl2zIeujt.IorFyCtsBSXsDMSKo59IMU4';
    function FinePageServices() {
        var _this = _super.call(this) || this;
        httpUtility = new HttpUtility_1.HttpUtility();
        return _this;
    }
    FinePageServices.prototype.fetchNameList = function () {
        var query = "SELECT+Name+FROM+Person__c";
        return httpUtility.getMethod(query, AppConstantValues_1.AppConstantValues.ACCESS_TOKEN);
    };
    FinePageServices.prototype.createDetailList = function (records) {
        var nameList = new observable_array_1.ObservableArray();
        for (var idx = 0; idx < records.length; idx++) {
            var singleDetail = { personId: '', personName: '' };
            singleDetail.personName = records[idx].Name;
            nameList.push(records[idx].Name);
        }
        return nameList;
    };
    FinePageServices.prototype.getAccessToken = function () {
        var _this = this;
        var body = AppConstantValues_1.AppConstantValues.ACCESS_TOKEN_BODY;
        var postResponse = httpUtility.postMethod(body);
        postResponse.then(function (response) {
            AppConstantValues_1.AppConstantValues.ACCESS_TOKEN = response.content.toJSON().access_token;
            _this.fetchNameList();
        }, function (error) {
            //// Argument (e) is Error!
            console.log(error);
        });
    };
    return FinePageServices;
}(observable_1.Observable));
exports.FinePageServices = FinePageServices;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRmluZVBhZ2VTZXJ2aWNlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkZpbmVQYWdlU2VydmljZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw4Q0FBa0U7QUFDbEUsMERBQXNEO0FBQ3RELDBEQUF1RDtBQUN2RCx5REFBc0Q7QUFHdEQsSUFBSSxXQUF5QixDQUFDO0FBRTlCO0lBQXNDLG9DQUFVO0lBRTVDLGtEQUFrRDtJQUNsRCxvSEFBb0g7SUFDcEgsdURBQXVEO0lBQ3ZELHVEQUF1RDtJQUN2RCx5RUFBeUU7SUFDekUscUpBQXFKO0lBRXJKO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBREcsV0FBVyxHQUFHLElBQUkseUJBQVcsRUFBRSxDQUFDOztJQUNwQyxDQUFDO0lBRU0sd0NBQWEsR0FBcEI7UUFDSSxJQUFJLEtBQUssR0FBRyw0QkFBNEIsQ0FBQztRQUN6QyxNQUFNLENBQUUsV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUMscUNBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUVNLDJDQUFnQixHQUF2QixVQUF3QixPQUFhO1FBQ2pDLElBQUksUUFBUSxHQUFHLElBQUksa0NBQWUsRUFBUyxDQUFDO1FBQzVDLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRyxHQUFHLEVBQUUsRUFBQyxDQUFDO1lBQ3pDLElBQUksWUFBWSxHQUFVLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUMsRUFBRSxFQUFDLENBQUM7WUFDdkQsWUFBWSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzVDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLENBQUM7UUFDRCxNQUFNLENBQUMsUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFFTSx5Q0FBYyxHQUFyQjtRQUFBLGlCQVVDO1FBVEcsSUFBSSxJQUFJLEdBQVUscUNBQWlCLENBQUMsaUJBQWlCLENBQUM7UUFDdEQsSUFBSSxZQUFZLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoRCxZQUFZLENBQUMsSUFBSSxDQUFFLFVBQUMsUUFBUTtZQUNwQixxQ0FBaUIsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxZQUFZLENBQUM7WUFDeEUsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQzdCLENBQUMsRUFBRSxVQUFDLEtBQUs7WUFDTCwyQkFBMkI7WUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTCx1QkFBQztBQUFELENBQUMsQUF6Q0QsQ0FBc0MsdUJBQVUsR0F5Qy9DO0FBekNZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7T2JzZXJ2YWJsZSwgZnJvbU9iamVjdCwgRXZlbnREYXRhfSBmcm9tICdkYXRhL29ic2VydmFibGUnO1xyXG5pbXBvcnQge09ic2VydmFibGVBcnJheX0gZnJvbSAnZGF0YS9vYnNlcnZhYmxlLWFycmF5JztcclxuaW1wb3J0IHtIdHRwVXRpbGl0eX0gZnJvbSAnLi4vSHR0cFV0aWxpdHkvSHR0cFV0aWxpdHknO1xyXG5pbXBvcnQge0FwcENvbnN0YW50VmFsdWVzfSBmcm9tICcuL0FwcENvbnN0YW50VmFsdWVzJztcclxuaW1wb3J0IGh0dHAgPSByZXF1aXJlKCdodHRwJyk7XHJcblxyXG5sZXQgaHR0cFV0aWxpdHkgOiBIdHRwVXRpbGl0eTtcclxuIFxyXG5leHBvcnQgY2xhc3MgRmluZVBhZ2VTZXJ2aWNlcyBleHRlbmRzIE9ic2VydmFibGUge1xyXG5cclxuICAgIC8vTmVjZXNzYXJ5IHZhcmlhYmxlcyByZXFpcmVkIGZvciB0aGUgQWNjZXNzIHRva2VuXHJcbiAgICAvLyBwcml2YXRlIGNsaWVudElkOnN0cmluZz0gJzNNVkc5WkwwcHBHUDVVckFNeWxzUGtTejBtQVdIX2pOYlhvRmVrb2dKRVhoUWhudDJaWXFjQU9uMUprenpuMXFTdlYxM0czWUZkMzhGaUVUMEZYRjQnO1xyXG4gICAgLy8gcHJpdmF0ZSBjbGllbnRTZWNyZXQ6c3RyaW5nID0gJzg1MzY5ODA1OTAzNDE4MDA4NjUnO1xyXG4gICAgLy8gcHJpdmF0ZSB1c2VyTmFtZTogc3RyaW5nICA9ICd0YXJpcXVlQHRyYWlsaGVhZC5jb20nO1xyXG4gICAgLy8gcHJpdmF0ZSBwYXNzd29yZDogc3RyaW5nID0nJHJheHpAbHVjaWZlcjc2OUZZTVpTUE0wNFBza0Jtd01ZN0RNUXAySUEnO1xyXG4gICAgLy8gcHJpdmF0ZSBhY2Nlc3NUb2tlbiA6IHN0cmluZyA9ICcwMEQyODAwMDAwMWlBdXkhQVFzQVFLT3kuWVhsMWl2WmV1c3I2VHhpUzJTVW1KVHBUQ2FBeUxPdy53SmJxNy5WaGh4d09kb0NZTENNeWhTVWwyeklldWp0LklvckZ5Q3RzQlNYc0RNU0tvNTlJTVU0JztcclxuICAgIFxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBodHRwVXRpbGl0eSA9IG5ldyBIdHRwVXRpbGl0eSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBmZXRjaE5hbWVMaXN0KCkgOiBQcm9taXNlPGh0dHAuSHR0cFJlc3BvbnNlPntcclxuICAgICAgICB2YXIgcXVlcnkgPSBcIlNFTEVDVCtOYW1lK0ZST00rUGVyc29uX19jXCI7XHJcbiAgICAgICAgcmV0dXJuICBodHRwVXRpbGl0eS5nZXRNZXRob2QocXVlcnksQXBwQ29uc3RhbnRWYWx1ZXMuQUNDRVNTX1RPS0VOKTtcclxuICAgIH1cclxuIFxyXG4gICAgcHVibGljIGNyZWF0ZURldGFpbExpc3QocmVjb3JkcyA6IGFueSkgOiBPYnNlcnZhYmxlQXJyYXk8SU5hbWU+e1xyXG4gICAgICAgIGxldCBuYW1lTGlzdCA9IG5ldyBPYnNlcnZhYmxlQXJyYXk8SU5hbWU+KCk7XHJcbiAgICAgICAgZm9yKHZhciBpZHggPSAwOyBpZHg8cmVjb3Jkcy5sZW5ndGggOyBpZHgrKyl7IFxyXG4gICAgICAgICAgICB2YXIgc2luZ2xlRGV0YWlsIDpJTmFtZSA9IHtwZXJzb25JZDonJywgcGVyc29uTmFtZTonJ307XHJcbiAgICAgICAgICAgIHNpbmdsZURldGFpbC5wZXJzb25OYW1lID0gcmVjb3Jkc1tpZHhdLk5hbWU7XHJcbiAgICAgICAgICAgIG5hbWVMaXN0LnB1c2gocmVjb3Jkc1tpZHhdLk5hbWUpO1xyXG4gICAgICAgIH0gICAgIFxyXG4gICAgICAgIHJldHVybiBuYW1lTGlzdDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0QWNjZXNzVG9rZW4oKXtcclxuICAgICAgICB2YXIgYm9keTpzdHJpbmcgPSBBcHBDb25zdGFudFZhbHVlcy5BQ0NFU1NfVE9LRU5fQk9EWTtcclxuICAgICAgICB2YXIgcG9zdFJlc3BvbnNlID0gaHR0cFV0aWxpdHkucG9zdE1ldGhvZChib2R5KTtcclxuICAgICAgICBwb3N0UmVzcG9uc2UudGhlbiggKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBBcHBDb25zdGFudFZhbHVlcy5BQ0NFU1NfVE9LRU4gPSByZXNwb25zZS5jb250ZW50LnRvSlNPTigpLmFjY2Vzc190b2tlbjtcclxuICAgICAgICAgICAgICAgIHRoaXMuZmV0Y2hOYW1lTGlzdCgpO1xyXG4gICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAvLy8vIEFyZ3VtZW50IChlKSBpcyBFcnJvciFcclxuICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgXHJcbn0iXX0=