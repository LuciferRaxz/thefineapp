import {Observable, fromObject, EventData} from 'data/observable';
import {ObservableArray} from 'data/observable-array';
import {HttpUtility} from '../HttpUtility/HttpUtility'
import http = require('http');
import {ListPageModel} from '../views/listpage/ListPageModel';
import {AppConstantValues} from './AppConstantValues';



let httpUtility : HttpUtility;
  
export class ListPageServices extends Observable {

    //Necessary variables reqired for the Access token
    // private clientId:string= '3MVG9ZL0ppGP5UrAMylsPkSz0mAWH_jNbXoFekogJEXhQhnt2ZYqcAOn1Jkzzn1qSvV13G3YFd38FiET0FXF4';
    // private clientSecret:string = '8536980590341800865';
    // private userName: string  = 'tarique@trailhead.com';
    // private password: string ='$raxz@lucifer769FYMZSPM04PskBmwMY7DMQp2IA';
    // private accessToken : string = '00D28000001iAuy!AQsAQKOy.YXl1ivZeusr6TxiS2SUmJTpTCaAyLOw.wJbq7.VhhxwOdoCYLCMyhSUl2zIeujt.IorFyCtsBSXsDMSKo59IMU4';
    private _data:ObservableArray<IPerson>;
    constructor() {
        super();
        httpUtility = new HttpUtility();
    }

    set data(value:ObservableArray<IPerson>) {
        this._data = value;
        this.notifyPropertyChange('data', value);
    }
    get data() {
        return this._data;
    }

    public fetchListOfPersons() : Promise<http.HttpResponse>{
        let personList = new ObservableArray<IPerson>();
        var query = "SELECT+id,name,Total_Fine__c+from+Person__c";
        return  httpUtility.getMethod(query,AppConstantValues.ACCESS_TOKEN);
    }
 
    public createListOfPersons(records : any) : ObservableArray<IPerson>{
        let personList = new ObservableArray<IPerson>();
        for(var idx = 0; idx<records.length ; idx++){ 
            var singlePerson :IPerson = {personId: '', personTotalFine:0, personName:''};
            singlePerson.personId = records[idx].Id;
            singlePerson.personName = records[idx].Name;
            singlePerson.personTotalFine = records[idx].Total_Fine__c;
            personList.push(singlePerson);
        }     
        return personList;
    }

    public getAccessToken(){
        var body:string = AppConstantValues.ACCESS_TOKEN_BODY;
        var postResponse = httpUtility.postMethod(body);
        postResponse.then( (response) => {
                AppConstantValues.ACCESS_TOKEN = response.content.toJSON().access_token;
                this.fetchListOfPersons();
        }, (error) => {
            //// Argument (e) is Error!
            console.log(error);
        });
    }
    
}