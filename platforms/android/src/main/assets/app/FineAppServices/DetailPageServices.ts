import {Observable, fromObject, EventData} from 'data/observable';
import {ObservableArray} from 'data/observable-array';
import {HttpUtility} from '../HttpUtility/HttpUtility';
import {AppConstantValues} from './AppConstantValues';
import http = require('http');

let httpUtility : HttpUtility;
 
export class DetailPageServices extends Observable {

    //Necessary variables reqired for the Access token
    // private clientId:string= '3MVG9ZL0ppGP5UrAMylsPkSz0mAWH_jNbXoFekogJEXhQhnt2ZYqcAOn1Jkzzn1qSvV13G3YFd38FiET0FXF4';
    // private clientSecret:string = '8536980590341800865';
    // private userName: string  = 'tarique@trailhead.com';
    // private password: string ='$raxz@lucifer769FYMZSPM04PskBmwMY7DMQp2IA';
    // private accessToken : string = '00D28000001iAuy!AQsAQKOy.YXl1ivZeusr6TxiS2SUmJTpTCaAyLOw.wJbq7.VhhxwOdoCYLCMyhSUl2zIeujt.IorFyCtsBSXsDMSKo59IMU4';
    private personId : String;
  
    constructor(personId : String) {
        super(); 
        this.personId = personId;
        httpUtility = new HttpUtility();
    }

    public fetchDetailList() : Promise<http.HttpResponse>{
        console.log(this.personId);
        var query = "SELECT+Fine_Amount__c,Name,Created_By__r.name,Person__r.Name+FROM+Fine__c+WHERE+Person__r.Id='"+this.personId+"'";
        return  httpUtility.getMethod(query,AppConstantValues.ACCESS_TOKEN);
    }
 
    public createDetailList(records : any) : ObservableArray<IDetail>{
        
        let detailList = new ObservableArray<IDetail>();
        for(var idx = 0; idx<records.length ; idx++){ 
            var singleDetail :IDetail = {fineAmount: 0, createdByPerson:'', personName:''};
            singleDetail.fineAmount = records[idx].Fine_Amount__c;
            singleDetail.createdByPerson = records[idx].Created_By__r.Name;
            singleDetail.personName = records[idx].Person__r.Name;
            detailList.push(singleDetail);
        }    
        detailList.forEach(element => {
            console.log('Person Name :: '+element.personName);
            console.log('Fine Amount :: '+element.fineAmount);
        });
        return detailList;
    }

    public getAccessToken(){
        var body:string = AppConstantValues.ACCESS_TOKEN_BODY;
        var postResponse = httpUtility.postMethod(body);
        postResponse.then( (response) => {
                AppConstantValues.ACCESS_TOKEN = response.content.toJSON().access_token;
                this.fetchDetailList();
        }, (error) => {
            //// Argument (e) is Error!
            console.log(error);
        });
    }
    
}