import {Observable, fromObject, EventData} from 'data/observable';
import {ObservableArray} from 'data/observable-array';
import {HttpUtility} from '../HttpUtility/HttpUtility';
import {AppConstantValues} from './AppConstantValues';
import http = require('http');

let httpUtility : HttpUtility;
 
export class FinePageServices extends Observable {

    //Necessary variables reqired for the Access token
    // private clientId:string= '3MVG9ZL0ppGP5UrAMylsPkSz0mAWH_jNbXoFekogJEXhQhnt2ZYqcAOn1Jkzzn1qSvV13G3YFd38FiET0FXF4';
    // private clientSecret:string = '8536980590341800865';
    // private userName: string  = 'tarique@trailhead.com';
    // private password: string ='$raxz@lucifer769FYMZSPM04PskBmwMY7DMQp2IA';
    // private accessToken : string = '00D28000001iAuy!AQsAQKOy.YXl1ivZeusr6TxiS2SUmJTpTCaAyLOw.wJbq7.VhhxwOdoCYLCMyhSUl2zIeujt.IorFyCtsBSXsDMSKo59IMU4';
    
    constructor() {
        super();
        httpUtility = new HttpUtility();
    }

    public fetchNameList() : Promise<http.HttpResponse>{
        var query = "SELECT+Name+FROM+Person__c";
        return  httpUtility.getMethod(query,AppConstantValues.ACCESS_TOKEN);
    }
 
    public createDetailList(records : any) : ObservableArray<IName>{
        let nameList = new ObservableArray<IName>();
        for(var idx = 0; idx<records.length ; idx++){ 
            var singleDetail :IName = {personId:'', personName:''};
            singleDetail.personName = records[idx].Name;
            nameList.push(records[idx].Name);
        }     
        return nameList;
    }

    public getAccessToken(){
        var body:string = AppConstantValues.ACCESS_TOKEN_BODY;
        var postResponse = httpUtility.postMethod(body);
        postResponse.then( (response) => {
                AppConstantValues.ACCESS_TOKEN = response.content.toJSON().access_token;
                this.fetchNameList();
        }, (error) => {
            //// Argument (e) is Error!
            console.log(error);
        });
    }
    
}