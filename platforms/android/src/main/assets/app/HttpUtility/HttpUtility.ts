import http = require('http');

export class HttpUtility{
    public getMethod(query : string, accessToken : String) : Promise<http.HttpResponse>{
        return http.request({ 
            url: "https://concretio-dev-ed.my.salesforce.com/services/data/v20.0/query/?q="+query,
            method: "GET" ,
            headers:
                {
                    'Authorization':'Bearer '+accessToken
                }
            });
    }
 
    public postMethod(body : string) : Promise<http.HttpResponse>{
        return http.request({ 
            url: "https://login.salesforce.com/services/oauth2/token",
            method: "POST" ,
            content: encodeURI(body)
        });
    }
}